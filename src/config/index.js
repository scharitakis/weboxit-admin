
import ApolloClient from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';


const cache = new InMemoryCache()

const initialState = {
    counter: 0
}


// or get from process.env.REACT_APP_{var} to handle PROD and DEV environments
export const APP_VERSION = '2.0.0';
export const API_BASE_URL = '/api';
export const ENABLE_REDUX_LOGGER = false;

export  const client = new ApolloClient({
    uri: '/.netlify/functions/fauna-graphql',
    request: (operation) => {
      const token = JSON.parse(localStorage.getItem('gotrue.user')).token.access_token;
      operation.setContext({
        headers: {
          authorization: token ? `Bearer ${token}` : ''
        }
      })
    },
    cache
  })

// export const client = new ApolloClient({
//     uri: '/.netlify/functions/fauna-graphql',
//     cache
// }); //setting up client

cache.writeData({  data: initialState });

export default {};

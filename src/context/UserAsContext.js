import React, {
  createContext,
  useState,
  useEffect
} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { storeUserAs } from 'src/utils/userAs';

const UserAsContext = createContext();

const defaultUserAsData = {
  userAs: null,
};

export function UserAsProvider({ userAs, children }) {
  const [currentUserAs, setCurrentUserAs] = useState(userAs || defaultUserAsData);

  const handleSetUserAs = (updatedUserAs = {}) => {
    const mergedUserAs = _.merge({}, currentUserAs, updatedUserAs);

    setCurrentUserAs(mergedUserAs);
    storeUserAs(mergedUserAs);
  };

  // useEffect(() => {
  //   document.dir = currentUserAs.userAs;
  // }, [currentUserAs]);

  return (
    <UserAsContext.Provider
      value={{
        userAs: currentUserAs,
        setUserAs: handleSetUserAs
      }}
    >
      {children}
    </UserAsContext.Provider>
  );
}

UserAsProvider.propTypes = {
  children: PropTypes.node.isRequired,
  userAs: PropTypes.object
};

export const UserAsConsumer = UserAsContext.Consumer;

export default UserAsContext;

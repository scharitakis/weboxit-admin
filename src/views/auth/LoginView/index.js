import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useHistory } from 'react-router';
import {
  Avatar,
  Button,
  Box,
  Container,
  Card,
  CardContent,
  CardMedia,
  Divider,
  Link,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import LockIcon from '@material-ui/icons/Lock';
import Page from 'src/components/Page';
import Logo from 'src/components/Logo';
import LoginForm from './LoginForm';
import { useDispatch } from 'react-redux';
import { useIdentityContext } from 'react-netlify-identity-widget'
import { setUserData, logout } from 'src/actions/accountActions';

import AuthStatusView from '../AuthStatusView'

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: 'center',
    backgroundColor: theme.palette.background.dark,
    display: 'flex',
    height: '100%',
    minHeight: '100%',
    flexDirection: 'column',
    paddingBottom: 80,
    paddingTop: 80
  },
  backButton: {
    marginLeft: theme.spacing(2)
  },
  card: {
    overflow: 'visible',
    display: 'flex',
    position: 'relative',
    '& > *': {
      flexGrow: 1,
      flexBasis: '50%',
      width: '50%'
    }
  },
  content: {
    padding: theme.spacing(8, 4, 3, 4)
  },
  icon: {
    backgroundColor: colors.green[500],
    color: theme.palette.common.white,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    position: 'absolute',
    top: -32,
    left: theme.spacing(3),
    height: 64,
    width: 64
  },
  media: {
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    padding: theme.spacing(3),
    color: theme.palette.common.white,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  }
}));

function LoginView() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();

  const identity = useIdentityContext()

  const handleSubmitSuccess = (userObj) => {

    //console.log(userObj)

    const setUser = async () => {
      const user =  {
        id: userObj.id,
        avatar: '/static/images/avatars/avatar_main.png',
        bio: 'Administrator',
        canHire: false,
        country: 'USA',
        email: userObj.email,
        username: userObj.email,
        password: '',
        firstName: userObj.user_metadata.full_name,
        isPublic: true,
        lastName: '',
        phone: '+40 777666555',
        role: 'admin',
        state: 'New York',
        timezone: '4:32PM (GMT-4)'
      }
  
      await dispatch(setUserData(user))
      history.push('/app');
      
    }
    
    setUser();
    
  };

  return (
    <Page
      className={classes.root}
      title="Login"
    >
      <Container maxWidth="md">
        <Box
          mb={8}
          display="flex"
          alignItems="center"
        >
          <Link 
            href="https://www.weboxit.com"
           >
            <Logo />
          </Link>
          <Button
            component={Link}
            href="https://www.weboxit.com"
            className={classes.backButton}
          >
            Back to WEBOXIT
          </Button>
        </Box>
        <Card className={classes.card}>
          <CardContent className={classes.content}>
            <Avatar className={classes.icon}>
              <LockIcon fontSize="large" />
            </Avatar>
            <Typography
              variant="h2"
              color="textPrimary"
            >
              Sign in
            </Typography>
            <Typography
              variant="subtitle1"
              color="textSecondary"
            >
              Sign in on the internal platform
            </Typography>
            <Box mt={2}>
              <Alert
                severity="info"
              >
                <div>
                  Please login
                </div>
              </Alert>
            </Box>
            <Box mt={3}>
              <LoginForm onSubmitSuccess={handleSubmitSuccess} />
              {/* <AuthStatusView onSubmitSuccess={handleSubmitSuccess} /> */}
            </Box>
            <Box my={2}>
              <Divider />
            </Box>
            <Link
              component={RouterLink}
              to="/register"
              variant="body2"
              color="textSecondary"
            >
              Create new account
            </Link>
          </CardContent>
          <CardMedia
            className={classes.media}
            image="/static/images/auth.png"
            title="Cover"
          >
            <Typography
              color="inherit"
              variant="subtitle1"
            >
              Welcome to Weboxit Administration App
            </Typography>
            <Box
              alignItems="center"
              display="flex"
              mt={3}
            >
              <Avatar
                alt="Person"
                src="/static/images/avatars/avatar_main.png"
              />
              <Box ml={3}>
                <Typography
                  color="inherit"
                  variant="body1"
                >
                  Stavros Charitakis
                </Typography>
                <Typography
                  color="inherit"
                  variant="body2"
                >
                  Engineering
                </Typography>
              </Box>
            </Box>
          </CardMedia>
        </Card>
      </Container>
    </Page>
  );
}

export default LoginView;

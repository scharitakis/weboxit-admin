import React from 'react'

import { IdentityModal, useIdentityContext } from 'react-netlify-identity-widget'
import 'react-netlify-identity-widget/styles.css'
import "@reach/tabs/styles.css"
import {
    Box,
    Button,
    TextField,
    FormHelperText,
    makeStyles
  } from '@material-ui/core';
  import { useHistory } from 'react-router';

function AuthStatusView({onSubmitSuccess}) {
    const identity = useIdentityContext()
    const history = useHistory();
    const [dialog, setDialog] = React.useState(false)
    const name =
      (identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.full_name) || 'NoName'
    // const avatar_url = identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.avatar_url
    return (
      <div className="App">
        <header className="App-header">
          {identity && identity.isLoggedIn ? (
            null
          ) : (
                <Button
                    color="secondary"
                    fullWidth
                    size="large"
                    type="button"
                    variant="contained"
                    onClick={() => setDialog(true)}
                    >
                    Log In
                </Button>
            )}
  
          <IdentityModal
            showDialog={dialog}
            onCloseDialog={() => setDialog(false)}
            onLogin={(user) => { setDialog(false); onSubmitSuccess(user)}}
            onSignup={(user) =>{ history.push('/contactAdmin')}}
            onLogout={() => console.log('bye ', name)}
          />
        </header>
      </div>
    )
  }

  export default AuthStatusView;
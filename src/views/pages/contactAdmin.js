import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
  Box,
  Button,
  Container,
  Typography,
  useTheme,
  useMediaQuery,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';

import { useIdentityContext } from 'react-netlify-identity'


const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(3),
    paddingTop: 80,
    paddingBottom: 80
  },
  image: {
    maxWidth: '100%',
    width: 560,
    maxHeight: 300,
    height: 'auto'
  }
}));

const Logout = ()=>{
  const identity = useIdentityContext()
  
  //console.log(identity);

  const logmeout = async()=>{
    try{
    //console.log('sssssss')
    await identity.logoutUser()
    }catch(e){
      //console.log(e)
    }
  }
  
  if(identity.isLoggedIn){
    logmeout();
  }

  return null
}

function ContactAdmin() {
  const classes = useStyles();
  const theme = useTheme();
  const mobileDevice = useMediaQuery(theme.breakpoints.down('sm'));
  //console.log('rendering')
  return (
    <Page
      className={classes.root}
      title="404: Not found"
    >
      <Logout />
      <Container maxWidth="lg">
        <Typography
          align="center"
          variant={mobileDevice ? 'h4' : 'h1'}
          color="textPrimary"
        >
          Please contact an administrator to enable your account
        </Typography>
        <Box
          mt={6}
          display="flex"
          justifyContent="center"
        >
          <Button
            color="secondary"
            component={RouterLink}
            to="/"
            variant="outlined"
          >
            Back to home
          </Button>
        </Box>
      </Container>
    </Page>
  );
}

export default ContactAdmin;

import React, { useState } from 'react';
import {
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import LatestProjects from './LatestProjects';
import NewProjects from './NewProjects';
import PerformanceOverTime from './PerformanceOverTime';
import RealTime from './RealTime';
import RoiPerCustomer from './RoiPerCustomer';
import SystemHealth from './SystemHealth';
import TeamTasks from './TeamTasks';
import TodaysMoney from './TodaysMoney';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import TotalStats from './TotalStats';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  container: {
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 64,
      paddingRight: 64
    }
  }
}));

const GET_ALL_STATS = gql`    
   query AllStats {
    totalStats{
      totalBoxes
      totalAccounts
      totalPurchases
    }
}`;

function DashboardView() {
  const classes = useStyles();
  const queryRes = useQuery(GET_ALL_STATS, {});
  const [totalStats, setTotalStats] = useState(null)

  const { loading, error, data, refetch } = queryRes

  if(!totalStats && data ){
    setTotalStats(data.totalStats)
  }

  return (
    <Page
      className={classes.root}
      title="Dashboard"
    >
      <Container
        maxWidth={false}
        className={classes.container}
      >
        <Header />
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={3}
            sm={6}
            xs={12}
          >
             <TotalStats title="Total Accounts" value={totalStats? totalStats.totalAccounts : "N/A"} />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xs={12}
          >
            <TotalStats title="Total Boxes" value={totalStats? totalStats.totalBoxes : "N/A"} />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xs={12}
          >
             <TotalStats title="Total Purchases" value={totalStats? totalStats.totalPurchases : "N/A"} />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xs={12}
          >
            {/*<RoiPerCustomer /> */}
          </Grid>
         { /* <Grid
            item
            lg={3}
            xs={12}
          >
            <RealTime />
          </Grid>
          <Grid
            item
            lg={9}
            xs={12}
          >
            <PerformanceOverTime />
          </Grid>
          <Grid
            item
            lg={5}
            xl={4}
            xs={12}
          >
            <TeamTasks />
          </Grid>
          <Grid
            item
            lg={7}
            xl={8}
            xs={12}
          >
            <LatestProjects />
          </Grid>
         */}
        </Grid>
      </Container>
    </Page>
  );
}

export default DashboardView;

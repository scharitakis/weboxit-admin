import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  TextField,
  Switch
} from '@material-ui/core';
import CustomInfoComponent from 'src/components/CustomInfoComponent';

import * as Yup from 'yup';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const UPDATE_COURIER= gql`
  mutation UpdateCourier(
    $courierId: ID!
    $email: String!
    $fullname: String!
    $street: String!
    $zipCode: String!
    $city: String!
    $phone: String!
    $name: String!
    $pin: String!
    $isEnabled: Boolean

  ) {
    updateCourier(id: $courierId
      data: {
        name: $name
        contact: {
          fullname: $fullname
          address: { street: $street, zipCode: $zipCode, city: $city }
          phone: $phone
          email: $email
        }
        isEnabled:$isEnabled
        pin:$pin
      }
    ) {
      _id
      _ts
      name
      contact {
        fullname
        address {
          street
          zipCode
          city
        }
        phone
        email
      }
      pin
      isEnabled
    }
  }
`;


const tableConfig = {
  _id: {
    property: "_id",
    cellRenderer: (rowData)=> rowData._id,
    header: 'Account Id:'
  },
  _ts: {
    property: "_ts",
    cellRenderer: (rowData)=> rowData._ts,
    header: '_ts'
  },
  isEnabled: {
    property: "isEnabled",
    initialValue: (rowData)=> rowData.isEnabled,
    cellRenderer: (rowData)=> rowData.isEnabled?'Yes':'No',
    cellEditRenderer: (rowData, extData)=>(
      <Switch
        checked={extData.values.isEnabled}
        color="secondary"
        edge="start"
        name="isEnabled"
        onChange={extData.handleChange}
        value={extData.values.isEnabled}
      />
    ),
    header: 'Enabled'
  },
  name: {
    property: "name",
    cellRenderer: (rowData)=> rowData.name,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.name && extData.errors.name)}
        fullWidth
        helperText={extData.touched.name && extData.errors.name}
        margin="none"
        size="small"
        name="name"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.name}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('pin is required'),
    header: 'name'
  },
  pin: {
    property: "pin",
    cellRenderer: (rowData)=> rowData.pin,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.pin && extData.errors.pin)}
        fullWidth
        helperText={extData.touched.pin && extData.errors.pin}
        margin="none"
        size="small"
        name="pin"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.pin}
        variant="outlined"
    />),
    validation: Yup.string().max(5).required('pin is required'),
    header: 'pin'
  },
  fullname: {
    property: "fullname",
    cellRenderer: (rowData)=> rowData.contact.fullname,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.fullname && extData.errors.fullname)}
        fullWidth
        helperText={extData.touched.fullname && extData.errors.fullname}
        margin="none"
        size="small"
        name="fullname"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.fullname}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('Required'),
    header: 'fullname'
  },
  street: {
    property: "street",
    cellRenderer: (rowData)=> rowData.contact.address.street,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.street && extData.errors.street)}
        fullWidth
        helperText={extData.touched.street && extData.errors.street}
        margin="none"
        size="small"
        name="street"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.street}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('street is required'),
    header: 'street'
  },
  zipCode: {
    property: "zipCode",
    cellRenderer: (rowData)=> rowData.contact.address.zipCode,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.zipCode && extData.errors.zipCode)}
        fullWidth
        helperText={extData.touched.zipCode && extData.errors.zipCode}
        margin="none"
        size="small"
        name="zipCode"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.zipCode}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('zipCode is required'),
    header: 'zipCode'
  },
  city: {
    property: "city",
    cellRenderer: (rowData)=> rowData.contact.address.city,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.city && extData.errors.city)}
        fullWidth
        helperText={extData.touched.city && extData.errors.city}
        margin="none"
        size="small"
        name="city"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.city}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('city is required'),
    header: 'city'
  },
  phone: {
    property: "phone",
    cellRenderer: (rowData)=> rowData.contact.phone,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.phone && extData.errors.phone)}
        fullWidth
        helperText={extData.touched.phone && extData.errors.phone}
        margin="none"
        size="small"
        name="phone"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.phone}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('phone is required'),
    header: 'phone'
  },
  email: {
    property: "email",
    cellRenderer: (rowData)=> rowData.contact.email,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.phone && extData.errors.email)}
        fullWidth
        helperText={extData.touched.phone && extData.errors.email}
        margin="none"
        size="small"
        name="email"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.email}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('phone is required'),
    header: 'email'
  }
}


const CourierInfo = ({ editable, station, className, ...rest })=>{
  const [data, setData] = useState(station)
  const [isEditable, setIsEditable] = useState(false);

  const [updateCourier] = useMutation(UPDATE_COURIER, {
    onError: error => {
      setIsEditable(!isEditable)
    },
    onCompleted: async (data) => {
      console.log('data',data);
      setData(data.updateCourier)
      setIsEditable(!isEditable)
    },
  });

  const onSubmit = async (values, {
    setErrors,
    setStatus,
    setSubmitting,
    resetForm
  }) => {
    try {
      //resetForm({})
      const uData = { variables:{
        name: values.name,
        pin: values.pin,
        courierId: data._id,
        fullname: values.fullname,
        street: values.street, 
        zipCode: values.zipCode, 
        city: values.city,
        phone: values.phone,
        email: values.email,
        isEnabled: values.isEnabled
        }
      }
      updateCourier(uData);     
    } catch (error) {
      const message = (error.response && error.response.data.message) || 'Something went wrong';

      setStatus({ success: false });
      setErrors({ submit: message });
      setSubmitting(false);
    }
  }

  return <CustomInfoComponent 
    title="Courier info"
    className={className}
    data={data}
    setData={setData}
    onSubmitHandler={onSubmit}
    editable={!!editable}
    isEditable={isEditable}
    setIsEditable={setIsEditable}
    tableConfig={tableConfig}
  />
}

CourierInfo.propTypes = {
  className: PropTypes.string,
  station: PropTypes.object.isRequired
};

export default CourierInfo;

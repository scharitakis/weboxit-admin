import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Grid, makeStyles } from '@material-ui/core';
import CourierInfo from './CourierInfo';


const useStyles = makeStyles(() => ({
  root: {}
}));

function Details({ station, className, ...rest }) {
  const classes = useStyles();

  return (
    <Grid
      className={clsx(classes.root, className)}
      container
      spacing={3}
      {...rest}
    >
      <Grid
          item
          xs={12}
        >
          <CourierInfo station={station} editable />
      </Grid>
    </Grid>
  );
}

Details.propTypes = {
  className: PropTypes.string,
  account: PropTypes.object.isRequired
};

export default Details;

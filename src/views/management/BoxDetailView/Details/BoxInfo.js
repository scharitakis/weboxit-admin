import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  TextField
} from '@material-ui/core';
import CustomInfoComponent from 'src/components/CustomInfoComponent';

import * as Yup from 'yup';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const UPDATE_BOX = gql`
  mutation UpdateAccount(
    $accountId: ID!
    $email: String!
    $password: String!
    $fullname: String!
    $street: String!
    $zipCode: String!
    $city: String!
    $phone: String!
    $termsAccepted: Boolean!
  ) {
    updateAccount(id: $accountId
      data: {
        username: $email
        password: $password
        contact: {
          fullname: $fullname
          address: { street: $street, zipCode: $zipCode, city: $city }
          phone: $phone
          email: $email
        }
        termsAccepted: $termsAccepted
      }
    ) {
      _id
      _ts
      username
      password
      termsAccepted
      contact {
        fullname
        address {
          street
          zipCode
          city
        }
        phone
        email
      }
    }
  }
`;


const tableConfig = {
  _id: {
    property: "_id",
    cellRenderer: (rowData)=> rowData._id,
    header: 'Box Id:'
  },
  _ts: {
    property: "_ts",
    cellRenderer: (rowData)=> rowData._ts,
    header: 'Date'
  },
  size: {
    property: "size",
    cellRenderer: (rowData)=> rowData.size,
    header: 'Size'
  },
  deleted: {
    property: "isDeleted",
    cellRenderer: (rowData)=> rowData.isDeleted?"Yes":"No",
    header: 'Deleted'
  },
  status: {
    property: "status",
    cellRenderer: (rowData)=> rowData.status,
    header: 'Status'
  },
  deliveryAtHome:{
    property: "deliveryAtHome",
    cellRenderer: (rowData)=> rowData.deliveryAtHome?"Yes":"No",
    header: 'deliveryAtHome'
  },
  deliveryPoint:{
    property: "deliveryPoint",
    cellRenderer: (rowData)=> rowData.deliveryPoint? `${rowData.deliveryPoint.name} (${rowData.deliveryPoint._id})` :"",
    header: 'deliveryPoint'
  },
  pickupPoint:{
    property: "pickupPoint",
    cellRenderer: (rowData)=> rowData.deliveryAtHome? rowData.recipient.fullname :`${rowData.pickupPoint.name} (${rowData.pickupPoint._id})`,
    header: 'pickupPoint'
  }

}


const BoxInfo = ({ editable, box, className, ...rest })=>{
  const [data, setData] = useState(box)
  const [isEditable, setIsEditable] = useState(false);

  const [updateAccount] = useMutation(UPDATE_BOX, {
    onError: error => {
      setIsEditable(!isEditable)
    },
    onCompleted: async (data) => {
      console.log('data',data);
      setData(data.updateAccount)
      setIsEditable(!isEditable)
    },
  });

  const onSubmit = async (values, {
    setErrors,
    setStatus,
    setSubmitting,
    resetForm
  }) => {
    try {
      console.log(values)
      //resetForm({})
      const uData = { variables:{
        username: data.username,
        password: data.password,
        termsAccepted: true,
        accountId: data._id,
        fullname: values.fullname,
        street: values.street, 
        zipCode: values.zipCode, 
        city: values.city,
        phone: values.phone,
        email: data.username,
        }
      }
      updateAccount(uData);     
    } catch (error) {
      const message = (error.response && error.response.data.message) || 'Something went wrong';

      setStatus({ success: false });
      setErrors({ submit: message });
      setSubmitting(false);
    }
  }

  return <CustomInfoComponent 
    title="Box info"
    className={className}
    data={data}
    setData={setData}
    onSubmitHandler={onSubmit}
    editable={!!editable}
    isEditable={isEditable}
    setIsEditable={setIsEditable}
    tableConfig={tableConfig}
  />
}

BoxInfo.propTypes = {
  className: PropTypes.string,
  box: PropTypes.object.isRequired
};

export default BoxInfo;

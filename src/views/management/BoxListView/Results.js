/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import moment from 'moment';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  InputAdornment,
  TextField,
  Card,
  IconButton,
  SvgIcon,
  Tabs,
  Tab,
  Divider,
  makeStyles
} from '@material-ui/core';
import {
  Image as ImageIcon,
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';

import CustomTable from 'src/components/CustomTable'


const useStyles = makeStyles((theme) => ({
  root: {},
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  queryField: {
    width: 500
  },
  categoryField: {
    flexBasis: 200
  },
  availabilityField: {
    marginLeft: theme.spacing(2),
    flexBasis: 200
  },
  stockField: {
    marginLeft: theme.spacing(2)
  },
  shippableField: {
    marginLeft: theme.spacing(2)
  },
  imageCell: {
    fontSize: 0,
    width: 68,
    flexBasis: 68,
    flexGrow: 0,
    flexShrink: 0
  },
  image: {
    height: 68,
    width: 68
  }
}));

const tableConfig = {
  id: {
    property: "id",
    cellRenderer: (rowData,data)=> rowData._id,
    header: 'ID'
  },
  date: {
    property: "ts",
    cellRenderer: (rowData,data)=> {
      const d = moment.unix(rowData._ts/1000000);
      return d.toLocaleString()
    },
    header: 'date'
  },
  size: {
    property: "size",
    cellRenderer: (rowData,data)=> rowData.size,
    header: 'size'
  },
  status: {
    property: "status",
    cellRenderer: (rowData,data)=> rowData.status,
    header: 'Status'
  },
  deleted: {
    property: "deleted",
    cellRenderer: (rowData,data)=> rowData.isDeleted ? "Yes": "No",
    header: 'deleted'
  },
  actions:{
    property: "actions",
    cellRenderer: (rowData,data,extData)=>  (
    <RouterLink to={`/app/management/boxes/${rowData._id}`} >
      <IconButton>
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      </IconButton>
    </RouterLink>),
    header: 'Actions'
  }
}


const tabs = [
  {
    value: 'pending',
    label: 'pending'
  },
  {
    value: 'paid',
    label: 'paid'
  },
  {
    value: 'scanned',
    label: 'scanned'
  },
  {
    value: 'shipped',
    label: 'shipped'
  },
  {
    value: 'inTransit',
    label: 'inTransit'
  },
  {
    value: 'dropped',
    label: 'dropped'
  },
  {
    value: 'delivered',
    label: 'delivered'
  },
  {
    value: 'not_delivered',
    label: 'not_delivered'
  }
];


function applyFilters(data, query) {
  return data.filter((b) => {
    let matches = true;

    if (query) {
      const properties = ['_id'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (b[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    return matches;
  });
}



function Results({ className, data, handleTabsChange, currentTab, ...rest }) {
  const classes = useStyles();

  const [query, setQuery] = useState('');

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const filteredData= applyFilters(data, query);

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
       <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab
            key={tab.value}
            value={tab.value}
            label={tab.label}
          />
        ))}
      </Tabs>
      <Divider />
      <Box
        p={2}
        minHeight={56}
        display="flex"
        alignItems="center"
      >
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon
                  fontSize="small"
                  color="action"
                >
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search Box Id"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
      </Box>
     <CustomTable tableConfig={tableConfig} data={filteredData} extData={{}} />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  data: PropTypes.array
};

Results.defaultProps = {
  data: []
};

export default Results;

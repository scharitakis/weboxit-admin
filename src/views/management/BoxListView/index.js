import React, {
  useState
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import Page from 'src/components/Page';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Header from './Header';
import Results from './Results';
import LoadingScreen from 'src/components/LoadingScreen';
import useUserAs from 'src/hooks/useUserAs'
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

const GET_BOXES_BY_ACCOUNT_STATUS = gql`    
  query FindBoxByAccountNStatusRev($accountId:ID!, $status:BoxStatus!){
  findBoxByAccountNStatusRev(accountId:$accountId, status: $status, _size: 1000000){
    data{
      _id
      _ts
      status
    	size
      isDeleted
    }
  }
}`;

function BoxListView() {
  const classes = useStyles();
  const { userAs } = useUserAs();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar } = useSnackbar();
  const [resData, setResData] = useState(null);
  const [currentTab, setCurrentTab] = useState('pending')

  const handleTabsChange = (event, value)=>{
    setResData(null)
    setCurrentTab(value);
  }

  const queryRes = useQuery(GET_BOXES_BY_ACCOUNT_STATUS, {
    onCompleted:(data)=>{
      setResData(data.findBoxByAccountNStatusRev.data)
    },
    onError:(err)=>{
      setResData(null)
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
    },
    fetchPolicy:"no-cache",
    variables: {
      accountId: userAs.userAs._id,
      status : currentTab
    }
  });

  const { loading, error, data, refetch } = queryRes


  
  if (loading) {
    return <LoadingScreen />;
  }


  return (
    <Page
      className={classes.root}
      title="Stations List"
    >
      <Container maxWidth={false}>
        <Header />
        {resData && (
          <Box mt={3}>
            <Results data={resData} handleTabsChange={handleTabsChange} currentTab={currentTab} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default BoxListView;

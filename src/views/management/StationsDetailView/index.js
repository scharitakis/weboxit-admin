import React, {
  useState
} from 'react';
import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import Details from './Details';
import {
  useParams
} from "react-router-dom";
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

const FIND_STATION_BY_ID = gql`
query FindStationByID($stationId: ID!){
  findStationByID(id:$stationId){
      _id
      _ts
      name
      contact {
        fullname
        address {
          street
          zipCode
          city
        }
        phone
        email
      }
      isEnabled
      lat
      lng
      pin
  }
}`;

function StationsDetailView() {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  let { id } = useParams();

  const [currentTab, setCurrentTab] = useState('details');
  const tabs = [
    { value: 'details', label: 'Details' },
  ];

  const queryRes = useQuery(FIND_STATION_BY_ID, {
    onCompleted:(data)=>{
      console.log(data);
    },
    onError:(err)=>{
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
    },
    variables: {
      stationId: id
    }
  });

  const { data } = queryRes;

  const handleTabsChange = (event, value) => {
    setCurrentTab(value);
  };

  return (
    <Page
      className={classes.root}
      title="Account Details"
    >
      <Container maxWidth={false}>
        {data ?<Header account={data.findStationByID} /> : null}
        <Box mt={3}>
          <Tabs
            onChange={handleTabsChange}
            scrollButtons="auto"
            value={currentTab}
            variant="scrollable"
            textColor="secondary"
            className={classes.tabs}
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box mt={3}>
          {data ? <>
            {(currentTab === 'details') && <Details station={data.findStationByID} />} </>
            : null
          }
        </Box>
      </Container>
    </Page>
  );
}

export default StationsDetailView;

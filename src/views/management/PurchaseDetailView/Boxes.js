import React from 'react';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import {
  Card,
  CardHeader,
  Divider,
  IconButton,
  SvgIcon,
  makeStyles
} from '@material-ui/core';
import CustomTable from 'src/components/CustomTable';
import { Link as RouterLink } from 'react-router-dom';
import {
  ArrowRight as ArrowRightIcon
} from 'react-feather';

const useStyles = makeStyles(() => ({
  root: {}
}));

const tableConfig = {
  id: {
    property: "id",
    cellRenderer: (rowData,data)=> rowData._id,
    header: 'ID'
  },
  status: {
    property: "status",
    cellRenderer: (rowData,data)=> rowData.status,
    header: 'status'
  },
  _ts: {
    property: "_ts",
    cellRenderer: (rowData,data)=> rowData._ts,
    header: '_ts'
  },
  size: {
    property: "size",
    cellRenderer: (rowData,data)=> rowData.size,
    header: 'size'
  },
  shippingCost: {
    property: "shippingCost",
    cellRenderer: (rowData,data)=> rowData.shippingCost,
    header: 'shippingCost'
  },
  actions:{
    property: "actions",
    cellRenderer: (rowData,data,extData)=>  (
    <RouterLink to={`/app/management/boxes/${rowData._id}`} >
      <IconButton>
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      </IconButton>
    </RouterLink>),
    header: 'Actions'
  }
}


function Boxes({ purchase, className, ...rest }) {
  const classes = useStyles();
 
  if (!purchase) {
    return null;
  }

  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card>
        <CardHeader
          title="Purchase Boxes"
        />
        <Divider />
        <CustomTable tableConfig={tableConfig} data={purchase.boxes} extData={{}} />
      </Card>
    </div>
  );
}

Boxes.propTypes = {
  className: PropTypes.string,
  purchase: PropTypes.object.isRequired
};

export default Boxes;

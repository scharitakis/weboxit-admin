import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Card,
  CardHeader,
  Divider,
  Button,
  SvgIcon,
  Box,
  makeStyles 
} from '@material-ui/core';
import CustomInfoRenderer from 'src/components/CustomInfoRenderer';
import { Edit as EditIcon } from 'react-feather';

const useStyles = makeStyles((theme) => ({
  root: {},
  fontWeightMedium: {
    fontWeight: theme.typography.fontWeightMedium
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

const tableConfig = {
  _id: {
    property: "_id",
    cellRenderer: (rowData)=> rowData._id,
    header: 'Purchase Id:'
  },
  totalCost: {
    property: "totalCost",
    cellRenderer: (rowData)=> rowData.totalCost,
    header: 'Total Cost'
  },
  status: {
    property: "status",
    cellRenderer: (rowData)=> rowData.status,
    cellEditRenderer: (rowData)=> 'edit me',
    header: 'Status'
  },
  merchantRef: {
    property: "merchantRef",
    cellRenderer: (rowData)=> rowData.merchantRef ? rowData.merchantRef: '',
    header: 'Merchant Ref'
  }
}

function PurchaseInfo({ editable, purchase, className, ...rest }) {
  const classes = useStyles();
  const [isEditable, setIsEditable] = useState(false);

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="Purchase info" />
      <Divider />
      <CustomInfoRenderer isEditable={isEditable} classes={classes} tableConfig={tableConfig} data={purchase} extData={{}} />
      {editable ? <Box m={2}>
        <Button
            color="secondary"
            variant="contained"
            size="small"
            onClick={()=> {setIsEditable(!isEditable)}}
          >
            <SvgIcon
              fontSize="small"
              className={classes.actionIcon}
            >
              <EditIcon />
            </SvgIcon>
            Edit
       </Button>
  </Box>: null }
    </Card>
  );
}

PurchaseInfo.propTypes = {
  className: PropTypes.string,
  purchase: PropTypes.object.isRequired
};

export default PurchaseInfo;

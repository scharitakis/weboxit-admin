import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Card,
  CardHeader,
  Divider,
  makeStyles
} from '@material-ui/core';
import CustomInfoRenderer from 'src/components/CustomInfoRenderer';

const useStyles = makeStyles((theme) => ({
  root: {},
  fontWeightMedium: {
    fontWeight: theme.typography.fontWeightMedium
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

const tableConfig = {
  MerchantReference: {
    property: "MerchantReference",
    cellRenderer: (rowData)=> rowData.transTicket.MerchantReference,
    header: 'MerchantReference:'
  },
  Parameters: {
    property: "Parameters",
    cellRenderer: (rowData)=> rowData.transTicket.Parameters,
    header: 'Parameters'
  },
  Amount: {
    property: "Amount",
    cellRenderer: (rowData)=> rowData.transTicket.Amount,
    header: 'Amount'
  },
  TranTicket: {
    property: "TranTicket",
    cellRenderer: (rowData)=> rowData.transTicket.TranTicket,
    header: 'TranTicket'
  },
  ResultCode: {
    property: "ResultCode",
    cellRenderer: (rowData)=> rowData.transTicket.ResultCode,
    header: 'ResultCode'
  },
  ResultDescription: {
    property: "TranTicket",
    cellRenderer: (rowData)=> rowData.transTicket.ResultDescription,
    header: 'Result Description'
  },
  Timestamp: {
    property: "Timestamp",
    cellRenderer: (rowData)=> rowData.transTicket.Timestamp,
    header: 'Timestamp'
  }
}


function TransactionTicketInfo({ purchase, className, ...rest }) {
  const classes = useStyles();
  console.log('purchase',purchase)

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="Transaction Ticket Info" />
      <Divider />
      { purchase.transTicket? <CustomInfoRenderer classes={classes} tableConfig={tableConfig} data={purchase} extData={{}} /> : "No Data found"}
    </Card>
  );
}

TransactionTicketInfo.propTypes = {
  className: PropTypes.string,
  purchase: PropTypes.object.isRequired
};

export default TransactionTicketInfo;

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Grid, makeStyles } from '@material-ui/core';
import PurchaseInfo from './PurchaseInfo';
import TransactionResult from './TransactionResult';
import TransactionTicketInfo from './TransactionTicket';

const useStyles = makeStyles(() => ({
  root: {}
}));

function Details({ purchase, className, ...rest }) {
  const classes = useStyles();

  return (
    <Grid
      className={clsx(classes.root, className)}
      container
      spacing={3}
      {...rest}
    >
      <Grid
        item
        xs
      >
        <PurchaseInfo purchase={purchase} />
      </Grid>
    <Grid
        item
        xs
      >
        <TransactionTicketInfo purchase={purchase} />
      </Grid>
       <Grid
        item
        xs
      >
        <TransactionResult purchase={purchase} />
      </Grid>
      {/*<Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <OtherActions />
    </Grid>*/}
    </Grid>
  );
}

Details.propTypes = {
  className: PropTypes.string,
  purchase: PropTypes.object.isRequired
};

export default Details;

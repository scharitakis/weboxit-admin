import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Card,
  CardHeader,
  Divider,
  makeStyles
} from '@material-ui/core';
import CustomInfoRenderer from 'src/components/CustomInfoRenderer';

const useStyles = makeStyles((theme) => ({
  root: {},
  fontWeightMedium: {
    fontWeight: theme.typography.fontWeightMedium
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

const tableConfig = {
  TraceID: {
    property: "TraceID",
    cellRenderer: (rowData)=> rowData.transResponse.TraceID,
    header: 'TraceID:'
  },
  MerchantReference: {
    property: "MerchantReference",
    cellRenderer: (rowData)=> rowData.transResponse.MerchantReference,
    header: 'MerchantReference:'
  },
  Parameters: {
    property: "Parameters",
    cellRenderer: (rowData)=> rowData.transResponse.Parameters,
    header: 'Parameters'
  },
  CardType: {
    property: "CardType",
    cellRenderer: (rowData)=> rowData.transResponse.CardType,
    header: 'CardType'
  },
  SupportReferenceID: {
    property: "SupportReferenceID",
    cellRenderer: (rowData)=> rowData.transResponse.SupportReferenceID,
    header: 'SupportReferenceID'
  },
  ResultCode: {
    property: "ResultCode",
    cellRenderer: (rowData)=> rowData.transResponse.ResultCode,
    header: 'ResultCode'
  },
  ResultDescription: {
    property: "TranTicket",
    cellRenderer: (rowData)=> rowData.transResponse.ResultDescription,
    header: 'Result Description'
  },
  TransactionDateTime: {
    property: "TransactionDateTime",
    cellRenderer: (rowData)=> rowData.transResponse.TransactionDateTime,
    header: 'TransactionDateTime'
  },
  ResponseCode: {
    property: "ResponseCode",
    cellRenderer: (rowData)=> rowData.transResponse.ResponseCode,
    header: 'ResponseCode'
  },
  RetrievalRef: {
    property: "RetrievalRef",
    cellRenderer: (rowData)=> rowData.transResponse.RetrievalRef,
    header: 'RetrievalRef'
  },
  TransactionId: {
    property: "TransactionId",
    cellRenderer: (rowData)=> rowData.transResponse.TransactionId,
    header: 'TransactionId'
  },
  PaymentMethod: {
    property: "PaymentMethod",
    cellRenderer: (rowData)=> rowData.transResponse.PaymentMethod,
    header: 'PaymentMethod'
  },
  PackageNo: {
    property: "PackageNo",
    cellRenderer: (rowData)=> rowData.transResponse.PackageNo,
    header: 'PackageNo'
  }
}


function TransactionResult({ purchase, className, ...rest }) {
  const classes = useStyles();
  console.log('purchase',purchase)

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="Transaction Result Info" />
      <Divider />
      { 
        purchase.transResponse? <CustomInfoRenderer classes={classes} tableConfig={tableConfig} data={purchase} extData={{}} /> : 'No data found'
      }
    </Card>
  );
}

TransactionResult.propTypes = {
  className: PropTypes.string,
  purchase: PropTypes.object.isRequired
};

export default TransactionResult;

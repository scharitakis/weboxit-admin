import React, {
  useState
} from 'react';
import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import Details from './Details';
import Boxes from './Boxes';
import {
  useParams
} from "react-router-dom";
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

const FIND_PURCHASE_BY_ID = gql`
query FindPurchaseById($purchaseId: ID!) {
  findPurchaseByID(id:$purchaseId){
  	totalCost
    _ts
    _id
    status
    merchantRef
    transTicket{
      MinutesToExpiration
      MerchantReference
      Parameters
      Amount
      TranTicket
      ResultDescription
      Timestamp
      ResultCode
    }
    transResponse{
      TraceID
      MerchantReference
      ResponseCode
      CardType
      SupportReferenceID
      ResponseCode
      RetrievalRef
      ResultDescription
      TransactionId
      ResultCode
      PaymentMethod
      PackageNo
      TransactionDateTime
      Parameters
    }
    boxes{
      _id
      status
      _ts
			size
      shippingCost
    }
  }
}`;

function PurchaseDetailView() {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  let { id } = useParams();

  const [purchase, setPurchase] = useState(null);
  const [currentTab, setCurrentTab] = useState('details');
  const tabs = [
    { value: 'details', label: 'Details' },
    { value: 'boxes', label: 'Boxes' }
  ];

  const queryRes = useQuery(FIND_PURCHASE_BY_ID, {
    onCompleted:(data)=>{
      console.log(data);
      setPurchase(data.findPurchaseByID)
    },
    onError:(err)=>{
      setPurchase(null)
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
    },
    variables: {
      purchaseId: id
    }
  });

  const handleTabsChange = (event, value) => {
    setCurrentTab(value);
  };

  if (!purchase) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="Purchase Details"
    >
      <Container maxWidth={false}>
        <Header purchase={purchase} />
        <Box mt={3}>
          <Tabs
            onChange={handleTabsChange}
            scrollButtons="auto"
            value={currentTab}
            variant="scrollable"
            textColor="secondary"
            className={classes.tabs}
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box mt={3}>
         {currentTab === 'details' && <Details purchase={purchase} />}
          {currentTab === 'boxes' && <Boxes purchase={purchase} />}
        </Box>
      </Container>
    </Page>
  );
}

export default PurchaseDetailView;

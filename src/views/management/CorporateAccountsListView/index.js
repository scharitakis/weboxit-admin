import React, {
  useState,
  useEffect,
  useCallback
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import Page from 'src/components/Page';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Header from './Header';
import Results from './Results';
import LoadingScreen from 'src/components/LoadingScreen';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

const GET_ALL_ACCOUNTS = gql`    
   query GetAllAccounts{
    getAccountsByType(type:"CORP"){
      data{
        username
        _id
        _ts
        contact{
          fullname
          phone
          email
        }
      }
    }
  }`;

function AccountsListView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar } = useSnackbar();
  const [accounts, setAccounts] = useState(null);
  
  const queryRes = useQuery(GET_ALL_ACCOUNTS, {
    fetchPolicy: 'no-cache',
    onCompleted:(data)=>{
      setAccounts(data.getAccountsByType.data)
    },
    onError:(err)=>{
      setAccounts(null)
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
    }
  });

  const { loading, error, data, refetch } = queryRes
  
  if (loading) {
    return <LoadingScreen />;
  }

  return (
    <Page
      className={classes.root}
      title="Accounts List"
    >
      <Container maxWidth={false}>
        <Header />
        {accounts && (
          <Box mt={3}>
            <Results accounts={accounts} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default AccountsListView;

/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  IconButton,
  SvgIcon,
  Box,
  InputAdornment,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  Image as ImageIcon,
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
import Label from 'src/components/Label';
import useUserAs from 'src/hooks/useUserAs'
import CustomTable from 'src/components/CustomTable'

const useStyles = makeStyles((theme) => ({
  root: {},
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  queryField: {
    width: 500
  },
  categoryField: {
    flexBasis: 200
  },
  availabilityField: {
    marginLeft: theme.spacing(2),
    flexBasis: 200
  },
  stockField: {
    marginLeft: theme.spacing(2)
  },
  shippableField: {
    marginLeft: theme.spacing(2)
  },
  imageCell: {
    fontSize: 0,
    width: 68,
    flexBasis: 68,
    flexGrow: 0,
    flexShrink: 0
  },
  image: {
    height: 68,
    width: 68
  }
}));

const tableConfig = {
  id: {
    property: "id",
    cellRenderer: (rowData,data)=> rowData._id,
    header: 'ID'
  },
  fullname: {
    property: "fullname",
    cellRenderer: (rowData,data)=> rowData.contact.fullname,
    header: 'Name'
  },
  email: {
    property: "email",
    cellRenderer: (rowData,data)=> rowData.contact.email,
    header: 'Email'
  },
  phone: {
    property: "phone",
    cellRenderer: (rowData,data)=> rowData.contact.phone,
    header: 'Mobile Phone'
  },
  connect: {
    property: "connect",
    cellRenderer: (rowData,data,extData)=> (<Button onClick={()=>extData.setUserAs({userAs:rowData})}>Connect</Button>),
    header: 'Connect User'
  },
  actions:{
    property: "actions",
    cellRenderer: (rowData,data,extData)=>  (
    <RouterLink to={`/app/management/accounts/corporate/${rowData._id}`} >
      <IconButton>
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      </IconButton>
    </RouterLink>),
    header: 'Actions'
  }
}

function applyFilters(accounts, query) {
  return accounts.filter((customer) => {
    let matches = true;

    if (query) {
      const properties = ['email', 'fullname'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (customer.contact[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    return matches;
  });
}

function Results({ className, accounts, ...rest }) {
  const classes = useStyles();
  const { userAs, setUserAs } = useUserAs()

  const [query, setQuery] = useState('');

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const filteredAccounts = applyFilters(accounts, query);

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
       <Box
        p={2}
        minHeight={56}
        display="flex"
        alignItems="center"
      >
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon
                  fontSize="small"
                  color="action"
                >
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search account"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
      </Box>
     <CustomTable tableConfig={tableConfig} data={filteredAccounts} extData={{userAs,setUserAs}} />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  products: PropTypes.array
};

Results.defaultProps = {
  products: []
};

export default Results;

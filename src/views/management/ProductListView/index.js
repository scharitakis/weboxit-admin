import React, {
  useState,
  useEffect,
  useCallback
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import axios from 'src/utils/axios';
import Page from 'src/components/Page';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

const GET_ALL_STATIONS = gql`    
    query GetAllStations{
      allStations
      {
        data{
          name
        }
      }
    }`;

function ProductListView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const [products, setProducts] = useState(null);
  
  const queryRes = useQuery(GET_ALL_STATIONS, {
    //variables: queryVars
  });

  const { loading, error, data, refetch } = queryRes

  //console.log(data);


  const getProducts = useCallback(() => {
    axios
      .get('/api/management/products')
      .then((response) => {
        if (isMountedRef.current) {
          setProducts(response.data.products);
        }
      });
  }, [isMountedRef]);

  useEffect(() => {
    getProducts();
  }, [getProducts]);

  if (!products) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="Product List"
    >
      <Container maxWidth={false}>
        <Header />
        {products && (
          <Box mt={3}>
            <Results products={products} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default ProductListView;

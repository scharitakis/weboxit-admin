/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import moment from 'moment';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  Checkbox,
  InputAdornment,
  FormControlLabel,
  IconButton,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Tabs,
  Tab,
  Divider,
  makeStyles
} from '@material-ui/core';
import {
  Image as ImageIcon,
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';

import CustomTable from 'src/components/CustomTable'


const useStyles = makeStyles((theme) => ({
  root: {},
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  queryField: {
    width: 500
  },
  categoryField: {
    flexBasis: 200
  },
  availabilityField: {
    marginLeft: theme.spacing(2),
    flexBasis: 200
  },
  stockField: {
    marginLeft: theme.spacing(2)
  },
  shippableField: {
    marginLeft: theme.spacing(2)
  },
  imageCell: {
    fontSize: 0,
    width: 68,
    flexBasis: 68,
    flexGrow: 0,
    flexShrink: 0
  },
  image: {
    height: 68,
    width: 68
  }
}));

const tableConfig = {
  id: {
    property: "id",
    cellRenderer: (rowData,data)=> rowData._id,
    header: 'ID'
  },
  date: {
    property: "ts",
    cellRenderer: (rowData,data)=> {
      const d = moment.unix(rowData._ts/1000000);
      return d.toLocaleString()
    },
    header: 'date'
  },
  timeStamp: {
    property: "timeStamp",
    cellRenderer: (rowData,data)=> {
      return !rowData.transTicket? '' : moment(rowData.transTicket.Timestamp).toLocaleString()
    },
    header: 'Transaction Ticket Date'
  },
  totalCost: {
    property: "totalCost",
    cellRenderer: (rowData,data)=> rowData.totalCost,
    header: 'Total Cost'
  },
  status: {
    property: "status",
    cellRenderer: (rowData,data)=> rowData.status,
    header: 'Status'
  },
  actions:{
    property: "actions",
    cellRenderer: (rowData,data,extData)=>  (
    <RouterLink to={`/app/management/purchases/${rowData._id}`} >
      <IconButton>
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      </IconButton>
    </RouterLink>),
    header: 'Actions'
  }
}

const tabs = [
  {
    value: 'completed',
    label: 'Completed'
  },
  {
    value: 'pending',
    label: 'Pending'
  },
  {
    value: 'rejected',
    label: 'Rejected'
  }
];


function Results({ className, products, handleTabsChange, currentTab, ...rest }) {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
       <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab
            key={tab.value}
            value={tab.value}
            label={tab.label}
          />
        ))}
      </Tabs>
      <Divider />
     <CustomTable tableConfig={tableConfig} data={products} extData={{}} />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  products: PropTypes.array
};

Results.defaultProps = {
  products: []
};

export default Results;

import React, {
  useState,
  useEffect,
  useCallback
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import Page from 'src/components/Page';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Header from './Header';
import Results from './Results';
import LoadingScreen from 'src/components/LoadingScreen';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

const GET_ALL_STATIONS = gql`    
    query GetAllStations{
      allStations(_size:2000)
      {
        data{
          name
          _id
          _ts
          lat
          lng
          pin
          isEnabled
          contact {
            fullname
            address {
              street
              city
              zipCode
            }
            phone
            email
          }
        }
      }
    }`;

function StationsListView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar } = useSnackbar();
  const [stations, setStations] = useState(null);
  
  const queryRes = useQuery(GET_ALL_STATIONS, {
    fetchPolicy:'no-cache',
    onCompleted:(data)=>{
      setStations(data.allStations.data)
    },
    onError:(err)=>{
      setStations(null)
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
    }
  });

  const { loading, error, data, refetch } = queryRes


  
  if (loading) {
    return <LoadingScreen />;
  }


  return (
    <Page
      className={classes.root}
      title="Stations List"
    >
      <Container maxWidth={false}>
        <Header />
        {stations && (
          <Box mt={3}>
            <Results stations={stations} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default StationsListView;

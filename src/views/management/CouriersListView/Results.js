/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  IconButton,
  SvgIcon,
  Box,
  InputAdornment,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  Image as ImageIcon,
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
import CustomTable from 'src/components/CustomTable'

const useStyles = makeStyles((theme) => ({
  root: {},
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  queryField: {
    width: 500
  },
  categoryField: {
    flexBasis: 200
  },
  availabilityField: {
    marginLeft: theme.spacing(2),
    flexBasis: 200
  },
  stockField: {
    marginLeft: theme.spacing(2)
  },
  shippableField: {
    marginLeft: theme.spacing(2)
  },
  imageCell: {
    fontSize: 0,
    width: 68,
    flexBasis: 68,
    flexGrow: 0,
    flexShrink: 0
  },
  image: {
    height: 68,
    width: 68
  }
}));


const tableConfig = {
  id: {
    property: "id",
    cellRenderer: (rowData,data)=> rowData._id,
    header: 'ID'
  },
  name: {
    property: "name",
    cellRenderer: (rowData,data)=> rowData.name,
    header: 'name'
  },
  city: {
    property: "city",
    cellRenderer: (rowData,data)=> rowData.contact.address.city,
    header: 'city'
  },
  pin: {
    property: "pin",
    cellRenderer: (rowData,data)=> rowData.pin,
    header: 'pin'
  },
  isEnabled: {
    property: "isEnabled",
    cellRenderer: (rowData,data)=> rowData.isEnabled?"Yes":"No",
    header: 'Enabled'
  },
  actions:{
    property: "actions",
    cellRenderer: (rowData,data,extData)=>  (
    <RouterLink to={`/app/management/couriers/${rowData._id}`} >
      <IconButton>
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      </IconButton>
    </RouterLink>),
    header: 'Actions'
  }
}


function applyFilters(stations, query) {
  return stations.filter((station) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (station[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    return matches;
  });
}

function Results({ className, stations, ...rest }) {
  const classes = useStyles();

  const [query, setQuery] = useState('');

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const filteredStations = applyFilters(stations, query);
 

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
       <Box
        p={2}
        minHeight={56}
        display="flex"
        alignItems="center"
      >
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon
                  fontSize="small"
                  color="action"
                >
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search Couriers"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
      </Box>
     <CustomTable tableConfig={tableConfig} data={filteredStations} />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  products: PropTypes.array
};

Results.defaultProps = {
  products: []
};

export default Results;

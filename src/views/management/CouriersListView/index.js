import React, {
  useState,
  useEffect,
  useCallback
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import Page from 'src/components/Page';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Header from './Header';
import Results from './Results';
import LoadingScreen from 'src/components/LoadingScreen';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

const GET_ALL_COURIERS = gql`    
    query GetAllCouriers{
      allCouriers(_size:2000)
      {
        data{
          name
          _id
          _ts
          pin
          isEnabled
          contact {
            fullname
            address {
              street
              city
              zipCode
            }
            phone
            email
          }
        }
      }
    }`;

function CouriersListView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar } = useSnackbar();
  const [couriers, setCouriers] = useState(null);
  
  const queryRes = useQuery(GET_ALL_COURIERS, {
    fetchPolicy:'no-cache',
    onCompleted:(data)=>{
      setCouriers(data.allCouriers.data)
    },
    onError:(err)=>{
      setCouriers(null)
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
    }
  });

  const { loading, error, data, refetch } = queryRes


  
  if (loading) {
    return <LoadingScreen />;
  }


  return (
    <Page
      className={classes.root}
      title="Couriers List"
    >
      <Container maxWidth={false}>
        <Header />
        {couriers && (
          <Box mt={3}>
            <Results stations={couriers} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default CouriersListView;

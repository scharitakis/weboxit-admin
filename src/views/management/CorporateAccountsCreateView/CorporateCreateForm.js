import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Switch,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import wait from 'src/utils/wait';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useHistory } from "react-router-dom";
import weboxitWebAuth from 'src/components/GoTrue'


const useStyles = makeStyles(() => ({
  root: {}
}));


const CREATE_CORPORATE_ACCOUNT= gql`
mutation CreateAccount(
  $email: String!
  $password: String!
  $fullname: String!
  $street: String!
  $zipCode: String!
  $city: String!
  $phone: String!
  $companyName: String!
  $vat: String!
  $taxOffice: String!
  $POBox: String
  $isEnabled: Boolean!
) {
  createAccount(
    data: {
      username: $email
      password: $password
      contact: {
        fullname: $fullname
        address: { street: $street, zipCode: $zipCode, city: $city }
        phone: $phone
        email: $email
      }
      type: "CORP"
      isEnabled: $isEnabled
      companyInfo:{
          companyName:$companyName
          vat:$vat
          taxOffice: $taxOffice
          POBox: $POBox
        }
      termsAccepted: true
    }
  ) {
    _id
    username,
    password

  }
}
`;


function CorporateCreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  let history = useHistory();

  const [createCorporateAccount] = useMutation(CREATE_CORPORATE_ACCOUNT, {
    onError: error => {
      enqueueSnackbar('Corporate Account Creation Failed', {
        variant: 'error'
      });
    },
    onCompleted: async (data) => {
     
      weboxitWebAuth
      .signup(data.createAccount.username, data.createAccount.password, {
          aid: data.createAccount._id,
      })
      .then(response => {
        enqueueSnackbar('Corporate Account Created', {
          variant: 'success'
      });
      })
      .catch(error => {
        enqueueSnackbar('Corporate Account Creation Failed but DB updated', {
          variant: 'error'
        });
      })
      .finally(()=>{  
          history.push("/app/management/accounts/corporate");
      });
    }
  });

  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
        companyName: '',
        vat: '',
        taxOffice: '',
        POBox: '',
        isEnabled: false,
        fullname: '',
        street: '',
        city: '',
        zipCode: '',
        phone: '',
      }}
      validationSchema={Yup.object().shape({
        companyName: Yup.string().max(255).required('required'),
        isEnabled: Yup.bool(),
        vat: Yup.string().max(9).required('required'),
        taxOffice: Yup.string().max(255).required('required'),
        fullname: Yup.string().max(255).required('required'),
        street: Yup.string().max(255).required('required'),
        city: Yup.string().max(255).required('required'),
        zipCode: Yup.string().max(255).required('required'),
        phone: Yup.string().max(10).required('required'),
        password: Yup.string().max(20).required('required'),
        email: Yup.string().email('Must be a valid email').max(255),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          console.log(values)
          createCorporateAccount({variables:values})
          setStatus({ success: true });
          setSubmitting(false);
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >

          <Card>
            <CardContent>
            <Grid
                container
                spacing={3}
              >
                <Grid item  md={6}>
                  <Grid item container spacing={2}>
                  <Grid
                    item
                    md={12}
                   >
                    <TextField
                      error={Boolean(touched.companyName && errors.companyName)}
                      fullWidth
                      helperText={touched.companyName && errors.companyName}
                      label="Company Name"
                      name="companyName"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.companyName}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                  >
                  <TextField
                    error={Boolean(touched.vat && errors.vat)}
                    fullWidth
                    helperText={touched.vat && errors.vat}
                    label="VAT"
                    name="vat"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.vat}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={12}
                >
                  <TextField
                    error={Boolean(touched.taxOffice && errors.taxOffice)}
                    fullWidth
                    helperText={touched.taxOffice && errors.taxOffice}
                    label="Tax Office"
                    name="taxOffice"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.taxOffice}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={12}
                >
                    <TextField
                      error={Boolean(touched.zipCode && errors.zipCode)}
                      fullWidth
                      helperText={touched.zipCode && errors.zipCode}
                      label="zipCode"
                      name="zipCode"
                      required
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.zipCode}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                  >
                    <TextField
                      error={Boolean(touched.street && errors.street)}
                      fullWidth
                      helperText={touched.street && errors.street}
                      label="Street"
                      name="street"
                      required
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.street}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                  >
                    <TextField
                      error={Boolean(touched.city && errors.city)}
                      fullWidth
                      helperText={touched.city && errors.city}
                      label="City"
                      name="city"
                      required
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.city}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                  >
                    <TextField
                      error={Boolean(touched.POBox && errors.POBox)}
                      fullWidth
                      helperText={touched.POBox && errors.POBox}
                      label="PO Box"
                      name="POBox"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.POBox}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                </Grid>
                <Grid item  md={6}>
                  <Grid item container spacing={2}>
                    <Grid
                      item
                      md={12}
                    >
                      <TextField
                        error={Boolean(touched.fullname && errors.fullname)}
                        fullWidth
                        helperText={touched.fullname && errors.fullname}
                        label="Representative Full name"
                        name="fullname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                        value={values.fullname}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                    >
                      <TextField
                        error={Boolean(touched.email && errors.email)}
                        fullWidth
                        helperText={touched.email && errors.email}
                        label="Email (username)"
                        name="email"
                        required
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.email}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                    >
                      <TextField
                        error={Boolean(touched.password && errors.password)}
                        fullWidth
                        helperText={touched.password && errors.password}
                        label="Password"
                        name="password"
                        required
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.password}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                    >
                    <TextField
                      error={Boolean(touched.phone && errors.phone)}
                      fullWidth
                      helperText={touched.phone && errors.phone}
                      label="Phone"
                      name="phone"
                      required
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.phone}
                      variant="outlined"
                    />
                  </Grid>
                  </Grid>
                </Grid>
                <Grid item />
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Typography
                    variant="h5"
                    color="textPrimary"
                  >
                    Enable
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                  >
                    Enabling this will make the Account Available
                  </Typography>
                  <Switch
                    checked={values.isEnabled}
                    color="secondary"
                    edge="start"
                    name="isEnabled"
                    onChange={handleChange}
                    value={values.isEnabled}
                  />
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Create Account
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

CorporateCreateForm.propTypes = {
  className: PropTypes.string
};

export default CorporateCreateForm;

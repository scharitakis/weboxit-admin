import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  TextField
} from '@material-ui/core';
import CustomInfoComponent from 'src/components/CustomInfoComponent';

import * as Yup from 'yup';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const UPDATE_ACCOUNT = gql`
  mutation UpdateAccount(
    $accountId: ID!
    $email: String!
    $password: String!
    $fullname: String!
    $street: String!
    $zipCode: String!
    $city: String!
    $phone: String!
    $termsAccepted: Boolean!
  ) {
    updateAccount(id: $accountId
      data: {
        username: $email
        password: $password
        contact: {
          fullname: $fullname
          address: { street: $street, zipCode: $zipCode, city: $city }
          phone: $phone
          email: $email
        }
        termsAccepted: $termsAccepted
      }
    ) {
      _id
      _ts
      username
      password
      termsAccepted
      contact {
        fullname
        address {
          street
          zipCode
          city
        }
        phone
        email
      }
    }
  }
`;


const tableConfig = {
  _id: {
    property: "_id",
    cellRenderer: (rowData)=> rowData._id,
    header: 'Account Id:'
  },
  username: {
    property: "username",
    cellRenderer: (rowData)=> rowData.username,
    header: 'username'
  },
  fullname: {
    property: "fullname",
    cellRenderer: (rowData)=> rowData.contact.fullname,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.fullname && extData.errors.fullname)}
        fullWidth
        helperText={extData.touched.fullname && extData.errors.fullname}
        margin="none"
        size="small"
        name="fullname"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.fullname}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('Required'),
    header: 'fullname'
  },
  street: {
    property: "street",
    cellRenderer: (rowData)=> rowData.contact.address.street,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.street && extData.errors.street)}
        fullWidth
        helperText={extData.touched.street && extData.errors.street}
        margin="none"
        size="small"
        name="street"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.street}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('street is required'),
    header: 'street'
  },
  zipCode: {
    property: "zipCode",
    cellRenderer: (rowData)=> rowData.contact.address.zipCode,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.zipCode && extData.errors.zipCode)}
        fullWidth
        helperText={extData.touched.zipCode && extData.errors.zipCode}
        margin="none"
        size="small"
        name="zipCode"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.zipCode}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('zipCode is required'),
    header: 'zipCode'
  },
  city: {
    property: "city",
    cellRenderer: (rowData)=> rowData.contact.address.city,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.city && extData.errors.city)}
        fullWidth
        helperText={extData.touched.city && extData.errors.city}
        margin="none"
        size="small"
        name="city"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.city}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('city is required'),
    header: 'city'
  },
  phone: {
    property: "phone",
    cellRenderer: (rowData)=> rowData.contact.phone,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.phone && extData.errors.phone)}
        fullWidth
        helperText={extData.touched.phone && extData.errors.phone}
        margin="none"
        size="small"
        name="phone"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.phone}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('phone is required'),
    header: 'phone'
  },
  email: {
    property: "email",
    cellRenderer: (rowData)=> rowData.contact.email,
    header: 'email'
  }
}


const AccountInfo = ({ editable, account, className, ...rest })=>{
  const [data, setData] = useState(account)
  const [isEditable, setIsEditable] = useState(false);

  const [updateAccount] = useMutation(UPDATE_ACCOUNT, {
    onError: error => {
      setIsEditable(!isEditable)
    },
    onCompleted: async (data) => {
      console.log('data',data);
      setData(data.updateAccount)
      setIsEditable(!isEditable)
    },
  });

  const onSubmit = async (values, {
    setErrors,
    setStatus,
    setSubmitting,
    resetForm
  }) => {
    try {
      console.log(values)
      //resetForm({})
      const uData = { variables:{
        username: data.username,
        password: data.password,
        termsAccepted: true,
        accountId: data._id,
        fullname: values.fullname,
        street: values.street, 
        zipCode: values.zipCode, 
        city: values.city,
        phone: values.phone,
        email: data.username,
        }
      }
      updateAccount(uData);     
    } catch (error) {
      const message = (error.response && error.response.data.message) || 'Something went wrong';

      setStatus({ success: false });
      setErrors({ submit: message });
      setSubmitting(false);
    }
  }

  return <CustomInfoComponent 
    title="Account info"
    className={className}
    data={data}
    setData={setData}
    onSubmitHandler={onSubmit}
    editable={!!editable}
    isEditable={isEditable}
    setIsEditable={setIsEditable}
    tableConfig={tableConfig}
  />
}

AccountInfo.propTypes = {
  className: PropTypes.string,
  account: PropTypes.object.isRequired
};

export default AccountInfo;

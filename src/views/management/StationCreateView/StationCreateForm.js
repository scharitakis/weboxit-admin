import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Switch,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import wait from 'src/utils/wait';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useHistory } from "react-router-dom";


const useStyles = makeStyles(() => ({
  root: {}
}));


const CREATE_STATION = gql`
  mutation CreateStation(
    $name: String!
    $isEnabled: Boolean!
    $fullname: String!
    $street: String!
    $city: String!
    $zipCode: String!
    $lat: String! 
    $lng: String!
    $pin: String!
    $phone: String!
    $email: String

  ) {
    createStation(
      data: {
        name: $name
        contact: {
          fullname: $fullname
          address: { street: $street, zipCode: $zipCode, city: $city }
          phone: $phone
          email: $email
        }
        lat:$lat
        lng:$lng
        pin:$pin
        isEnabled:$isEnabled
      }
    ) {
      _id
      _ts
      name
      isEnabled
      contact {
        fullname
        address {
          street
          zipCode
          city
        }
        phone
        email
      }
      lat
      lng
      pin
    }
  }
`;


function StationCreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  let history = useHistory();

  const [createStation] = useMutation(CREATE_STATION, {
    onError: error => {
      enqueueSnackbar('Station Creation Failed', {
        variant: 'error'
      });
    },
    onCompleted: async (data) => {
      enqueueSnackbar('Station Created', {
          variant: 'success'
      });
      history.push("/app/management/stations");

    },
  });

  return (
    <Formik
      initialValues={{
        name: '',
        lat: '',
        lng: '',
        pin: '',
        isEnabled: true,
        fullname: '',
        street: '',
        city: '',
        zipCode: '',
        phone: '',
        email: '',
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('required'),
        lat: Yup.string().max(255).required('required'),
        lng: Yup.string().max(255).required('required'),
        isEnabled: Yup.bool(),
        pin: Yup.string().max(5).required('required'),
        fullname: Yup.string().max(255).required('required'),
        street: Yup.string().max(255).required('required'),
        city: Yup.string().max(255).required('required'),
        zipCode: Yup.string().max(255).required('required'),
        phone: Yup.string().max(10).required('required'),
        email: Yup.string().email('Must be a valid email').max(255),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          console.log(values)
          createStation({variables:values})
          setStatus({ success: true });
          setSubmitting(false);
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >

          <Card>
            <CardContent>
            <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Display Name"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.name}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.fullname && errors.fullname)}
                    fullWidth
                    helperText={touched.fullname && errors.fullname}
                    label="Full name"
                    name="fullname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.fullname}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.street && errors.street)}
                    fullWidth
                    helperText={touched.street && errors.street}
                    label="Street"
                    name="street"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.street}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.city && errors.city)}
                    fullWidth
                    helperText={touched.city && errors.city}
                    label="City"
                    name="city"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.city}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.zipCode && errors.zipCode)}
                    fullWidth
                    helperText={touched.zipCode && errors.zipCode}
                    label="zipCode"
                    name="zipCode"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.zipCode}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.pin && errors.pin)}
                    fullWidth
                    helperText={touched.pin && errors.pin}
                    label="Pin"
                    name="pin"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.pin}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.lat && errors.lat)}
                    fullWidth
                    helperText={touched.lat && errors.lat}
                    label="Lat"
                    name="lat"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.lat}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.lng && errors.lng)}
                    fullWidth
                    helperText={touched.lng && errors.lng}
                    label="Lng"
                    name="lng"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.lng}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.phone && errors.phone)}
                    fullWidth
                    helperText={touched.phone && errors.phone}
                    label="Phone"
                    name="phone"
                    required
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.phone}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.email && errors.email)}
                    fullWidth
                    helperText={touched.email && errors.email}
                    label="Email"
                    name="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.email}
                    variant="outlined"
                  />
                </Grid>
                <Grid item />
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Typography
                    variant="h5"
                    color="textPrimary"
                  >
                    Enable
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                  >
                    Enabling this will make the station Available
                  </Typography>
                  <Switch
                    checked={values.isEnabled}
                    color="secondary"
                    edge="start"
                    name="isEnabled"
                    onChange={handleChange}
                    disabled
                    value={values.isEnabled}
                  />
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Create Station
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

StationCreateForm.propTypes = {
  className: PropTypes.string
};

export default StationCreateForm;

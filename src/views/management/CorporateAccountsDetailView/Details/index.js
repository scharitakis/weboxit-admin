import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Grid, makeStyles } from '@material-ui/core';
import AccountInfo from './AccountInfo';


const useStyles = makeStyles(() => ({
  root: {}
}));

function Details({ account, className, ...rest }) {
  const classes = useStyles();

  return (
    <Grid
      className={clsx(classes.root, className)}
      container
      spacing={3}
      {...rest}
    >
   <Grid
        item
        xs={12}
      >
        <AccountInfo account={account} editable />
      </Grid>
    </Grid>
  );
}

Details.propTypes = {
  className: PropTypes.string,
  account: PropTypes.object.isRequired
};

export default Details;

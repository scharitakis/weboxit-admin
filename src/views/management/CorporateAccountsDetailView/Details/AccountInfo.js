import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  TextField,
  Switch
} from '@material-ui/core';
import CustomInfoComponent from 'src/components/CustomInfoComponent';

import * as Yup from 'yup';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const UPDATE_ACCOUNT = gql`
  mutation UpdateAccount(
    $accountId: ID!
    $email: String!
    $password: String!
    $fullname: String!
    $street: String!
    $zipCode: String!
    $city: String!
    $phone: String!
    $termsAccepted: Boolean!
    $companyName: String!
    $vat: String!
    $taxOffice: String!
    $POBox:String
    $isEnabled:Boolean!
    $type:String!
  ) {
    updateAccount(id: $accountId
      data: {
        username: $email
        password: $password
        contact: {
          fullname: $fullname
          address: { street: $street, zipCode: $zipCode, city: $city }
          phone: $phone
          email: $email
        }
        companyInfo:{
            companyName: $companyName,
            vat: $vat,
            taxOffice: $taxOffice,
            POBox: $POBox
          }
        termsAccepted: $termsAccepted
        isEnabled: $isEnabled
        type:$type
      }
    ) {
      _id
      _ts
      username
      password
      termsAccepted
      type
      isEnabled
      contact {
        fullname
        address {
          street
          zipCode
          city
        }
        phone
        email
      }
      companyInfo{
          companyName
          vat
          taxOffice
          POBox
        }
    }
  }
`;


const tableConfig = {
  _id: {
    property: "_id",
    cellRenderer: (rowData)=> rowData._id,
    header: 'Account Id:'
  },
  type: {
    property: "type",
    cellRenderer: (rowData)=> rowData.type,
    header: 'Account Type'
  },
  isEnabled: {
    property: "isEnabled",
    initialValue: (rowData)=> rowData.isEnabled,
    cellRenderer: (rowData)=> rowData.isEnabled?'Yes':'No',
    cellEditRenderer: (rowData, extData)=>(
      <Switch
        checked={extData.values.isEnabled}
        color="secondary"
        edge="start"
        name="isEnabled"
        onChange={extData.handleChange}
        value={extData.values.isEnabled}
      />
    ),
    header: 'Enabled'
  },
  companyName: {
    property: "companyName",
    cellRenderer: (rowData)=> rowData.companyInfo.companyName,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.companyName && extData.errors.companyName)}
        fullWidth
        helperText={extData.touched.companyName && extData.errors.companyName}
        margin="none"
        size="small"
        name="companyName"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.companyName}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('Required'),
    header: 'Company Name'
  },
  vat: {
    property: "vat",
    cellRenderer: (rowData)=> rowData.companyInfo.vat,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.vat && extData.errors.vat)}
        fullWidth
        helperText={extData.touched.vat && extData.errors.vat}
        margin="none"
        size="small"
        name="vat"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.vat}
        variant="outlined"
    />),
    validation: Yup.string().max(9).required('Required'),
    header: 'VAT'
  },
  taxOffice: {
    property: "taxOffice",
    cellRenderer: (rowData)=> rowData.companyInfo.taxOffice,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.taxOffice && extData.errors.taxOffice)}
        fullWidth
        helperText={extData.touched.taxOffice && extData.errors.taxOffice}
        margin="none"
        size="small"
        name="taxOffice"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.taxOffice}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('Required'),
    header: 'TAX Office'
  },
  POBox: {
    property: "POBox",
    cellRenderer: (rowData)=> rowData.companyInfo.POBox,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.POBox && extData.errors.POBox)}
        fullWidth
        helperText={extData.touched.POBox && extData.errors.POBox}
        margin="none"
        size="small"
        name="POBox"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.POBox}
        variant="outlined"
    />),
    header: 'POBox'
  },
  username: {
    property: "username",
    cellRenderer: (rowData)=> rowData.username,
    header: 'username'
  },
  fullname: {
    property: "fullname",
    cellRenderer: (rowData)=> rowData.contact.fullname,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.fullname && extData.errors.fullname)}
        fullWidth
        helperText={extData.touched.fullname && extData.errors.fullname}
        margin="none"
        size="small"
        name="fullname"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.fullname}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('Required'),
    header: 'fullname'
  },
  street: {
    property: "street",
    cellRenderer: (rowData)=> rowData.contact.address.street,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.street && extData.errors.street)}
        fullWidth
        helperText={extData.touched.street && extData.errors.street}
        margin="none"
        size="small"
        name="street"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.street}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('street is required'),
    header: 'street'
  },
  zipCode: {
    property: "zipCode",
    cellRenderer: (rowData)=> rowData.contact.address.zipCode,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.zipCode && extData.errors.zipCode)}
        fullWidth
        helperText={extData.touched.zipCode && extData.errors.zipCode}
        margin="none"
        size="small"
        name="zipCode"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.zipCode}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('zipCode is required'),
    header: 'zipCode'
  },
  city: {
    property: "city",
    cellRenderer: (rowData)=> rowData.contact.address.city,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.city && extData.errors.city)}
        fullWidth
        helperText={extData.touched.city && extData.errors.city}
        margin="none"
        size="small"
        name="city"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.city}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('city is required'),
    header: 'city'
  },
  phone: {
    property: "phone",
    cellRenderer: (rowData)=> rowData.contact.phone,
    cellEditRenderer: (rowData, extData)=>  ( 
      <TextField
        error={Boolean(extData.touched.phone && extData.errors.phone)}
        fullWidth
        helperText={extData.touched.phone && extData.errors.phone}
        margin="none"
        size="small"
        name="phone"
        onBlur={extData.handleBlur}
        onChange={extData.handleChange}
        type="text"
        value={extData.values.phone}
        variant="outlined"
    />),
    validation: Yup.string().max(255).required('phone is required'),
    header: 'phone'
  },
  email: {
    property: "email",
    cellRenderer: (rowData)=> rowData.contact.email,
    header: 'email'
  }
}


const AccountInfo = ({ editable, account, className, ...rest })=>{
  const [data, setData] = useState(account)
  const [isEditable, setIsEditable] = useState(false);

  const [updateAccount] = useMutation(UPDATE_ACCOUNT, {
    onError: error => {
      setIsEditable(!isEditable)
    },
    onCompleted: async (data) => {
      console.log('data',data);
      setData(data.updateAccount)
      setIsEditable(!isEditable)
    },
  });

  const onSubmit = async (values, {
    setErrors,
    setStatus,
    setSubmitting,
    resetForm
  }) => {
    try {
      console.log(values)
      //resetForm({})
      const uData = { variables:{
        username: data.username,
        password: data.password,
        termsAccepted: true,
        accountId: data._id,
        fullname: values.fullname,
        street: values.street, 
        zipCode: values.zipCode, 
        city: values.city,
        phone: values.phone,
        email: data.username,
        isEnabled: values.isEnabled,
        type: data.type,
        companyName: values.companyName,
        vat: values.vat,
        taxOffice: values.taxOffice,
        POBox: values.POBox,
        }
      }
      updateAccount(uData);     
    } catch (error) {
      const message = (error.response && error.response.data.message) || 'Something went wrong';

      setStatus({ success: false });
      setErrors({ submit: message });
      setSubmitting(false);
    }
  }

  return <CustomInfoComponent 
    title="Account info"
    className={className}
    data={data}
    setData={setData}
    onSubmitHandler={onSubmit}
    editable={!!editable}
    isEditable={isEditable}
    setIsEditable={setIsEditable}
    tableConfig={tableConfig}
  />
}

AccountInfo.propTypes = {
  className: PropTypes.string,
  account: PropTypes.object.isRequired
};

export default AccountInfo;

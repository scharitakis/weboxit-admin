import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import 'react-perfect-scrollbar/dist/css/styles.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import 'react-quill/dist/quill.snow.css';
import 'nprogress/nprogress.css';
import 'src/assets/css/prism.css';
import 'src/mixins/chartjs';
import 'src/mixins/prismjs';
import 'src/mock';
import { enableES5 } from 'immer';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/react-hooks';
import * as serviceWorker from 'src/serviceWorker';
import { SettingsProvider } from 'src/context/SettingsContext';
import { UserAsProvider } from 'src/context/UserAsContext';
import { restoreUserAs } from 'src/utils/userAs';
import { configureStore } from 'src/store';
import { restoreSettings } from 'src/utils/settings';
import { client } from 'src/config';
import App from 'src/App';
import { IdentityModal, useIdentityContext, IdentityContextProvider } from 'react-netlify-identity-widget'
import 'react-netlify-identity-widget/styles.css'
import "@reach/tabs/styles.css"

enableES5();

const store = configureStore();
const settings = restoreSettings();
const userAs = restoreUserAs();
const identityUrl = 'https://admin-weboxit.netlify.app'


ReactDOM.render(
  <ApolloProvider client={client}> 
    <Provider store={store}>
      <IdentityContextProvider url={identityUrl}>
        <SettingsProvider settings={settings}>
          <UserAsProvider userAs={userAs}>
            <App />
          </UserAsProvider>
        </SettingsProvider>
      </IdentityContextProvider>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();

export function restoreUserAs() {
  let userAs = null;

  try {
    const storedData = localStorage.getItem('userAs');

    if (storedData) {
      userAs = JSON.parse(storedData);
    }
  } catch (err) {
    // If stored data is not a strigified JSON this might fail,
    // that's why we catch the error
  }

  return userAs;
}

export function storeUserAs(data) {
  localStorage.setItem('userAs', JSON.stringify(data));
}


import { useContext } from 'react';
import UserAsContext from 'src/context/UserAsContext';

export default function useUserAs() {
  const context = useContext(UserAsContext);

  return context;
}

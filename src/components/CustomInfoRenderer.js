import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography
} from '@material-ui/core';

const CustomInfoRenderer = ({isEditable, classes, tableConfig, data, extData}) =>{
    const headers = Object.keys(tableConfig)
    return(
      <Table>
        <TableBody>
        { headers.map(h=> (
          <TableRow key={`${h}`}>
              <TableCell className={classes.fontWeightMedium}>
                {tableConfig[h].header}
              </TableCell>
              <TableCell>
                <Typography
                  variant="body2"
                  color="textSecondary"
                >
                  { isEditable ? 
                  (tableConfig[h].cellEditRenderer) ? tableConfig[h].cellEditRenderer(data, extData): tableConfig[h].cellRenderer(data, extData) : 
                  tableConfig[h].cellRenderer(data, extData)}
                </Typography>
              </TableCell>
            </TableRow>)
        )}
        </TableBody>
      </Table>)
  }

  export default CustomInfoRenderer;
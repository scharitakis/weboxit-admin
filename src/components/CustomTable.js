/* eslint-disable max-len */
import React, { useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';


const CustomTable = ({tableConfig, data, extData}) =>{
    const headers = Object.keys(tableConfig)
    return(
    <PerfectScrollbar>
    <Box minWidth={1200}>
      <Table>
        <TableHead>
          <TableRow>
          { headers.map(d=>(
             <TableCell key={`${d}`}>
             {tableConfig[d].header}
           </TableCell>
            ))
          }
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((rowData) => {
            return (
              <TableRow
                hover
                key={rowData._id}
              >
                {headers.map(h=>
                   (<TableCell key={`${h}-${rowData._id}`}>
                   {tableConfig[h].cellRenderer(rowData,data, extData)}
                  </TableCell>)
                  )}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Box>
  </PerfectScrollbar>)
  }
  

  export default CustomTable;


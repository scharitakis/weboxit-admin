import GoTrue from 'gotrue-js';

// Instantiate the GoTrue auth client with an optional configuration

const weboxitWebAuth = new GoTrue({
  APIUrl: 'https://www.weboxit.com/.netlify/identity',
  audience: '',
  setCookie: false,
});

export default weboxitWebAuth;

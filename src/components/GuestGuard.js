import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useIdentityContext } from 'react-netlify-identity'

function GuestGuard({ children }) {
  const account = useSelector((state) => state.account);
  const identity = useIdentityContext()
  
  if(identity.isLoggedIn){
    return <Redirect to="/app" />
  }

  return children;
}

GuestGuard.propTypes = {
  children: PropTypes.any
};

export default GuestGuard;

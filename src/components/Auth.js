import React, {
  useEffect,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import SplashScreen from 'src/components/SplashScreen';
import { setUserData, logout } from 'src/actions/accountActions';
import authService from 'src/services/authService';
import { useIdentityContext } from 'react-netlify-identity-widget'

function Auth({ children }) {
  const identity  = useIdentityContext()
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const initAuth = async () => {
      // authService.setAxiosInterceptors({
      //   onLogout: () => dispatch(logout())
      // });

      // authService.handleAuthentication();

      // if (authService.isAuthenticated()) {
      //   const user = await authService.loginInWithToken();

      //   await dispatch(setUserData(user));
      // }

      //console.log(identity)

      if(identity.isLoggedIn){
        const user =  {
          id: identity.user.id,
          avatar: '/static/images/avatars/avatar_main.png',
          bio: 'Administrator',
          canHire: false,
          country: 'USA',
          email: identity.user.email,
          username: identity.user.email,
          password: '',
          firstName: identity.user.user_metadata.full_name,
          isPublic: true,
          lastName: '',
          phone: '+40 777666555',
          role: 'admin',
          state: 'New York',
          timezone: '4:32PM (GMT-4)'
        }

        await dispatch(setUserData(user))
      }

      setLoading(false);
    };

    initAuth();
  }, [dispatch]);

  if (isLoading) {
    return <SplashScreen />;
  }

  return children;
}

Auth.propTypes = {
  children: PropTypes.any
};

export default Auth;

import React, { useState } from 'react';
import clsx from 'clsx';
import {
  Card,
  CardHeader,
  Divider,
  Button,
  SvgIcon,
  Box,
  Grid,
  makeStyles
} from '@material-ui/core';
import CustomInfoRenderer from 'src/components/CustomInfoRenderer';
import { Edit as EditIcon } from 'react-feather';
import { Formik } from 'formik';
import * as Yup from 'yup';
import SaveIcon from '@material-ui/icons/Save';

const useStyles = makeStyles((theme) => ({
    root: {},
    fontWeightMedium: {
      fontWeight: theme.typography.fontWeightMedium
    },
    actionIcon: {
      marginRight: theme.spacing(1)
    }
  }));

function CustomInfoComponent({ 
    editable,
    isEditable,
    setIsEditable,
    data,
    setData,
    title,
    onSubmitHandler,
    tableConfig,
    className, ...rest }) {
    const classes = useStyles();
   
    const formikConf = Object.keys(tableConfig).reduce((init, current)=>{
      if(!!tableConfig[current].cellEditRenderer){
        init.initValues[current] = tableConfig[current].initialValue? tableConfig[current].initialValue(data) : tableConfig[current].cellRenderer(data)
        init.validationSchema[current] = tableConfig[current].validation
      }
      return init
    },{initValues:{}, validationSchema:{}})
  
  
  
    return (
      <Card
        className={clsx(classes.root, className)}
        {...rest}
      >
        <CardHeader title={title} />
        <Divider />
        <Formik
          initialValues={formikConf.initValues}
          validationSchema={Yup.object().shape(
            formikConf.validationSchema
          )}
          onSubmit={onSubmitHandler}
        >
           {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          handleReset,
          isSubmitting,
          touched,
          values
        }) => (
          <form
            noValidate
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
        <CustomInfoRenderer isEditable={isEditable} classes={classes} tableConfig={tableConfig} data={data} extData={{
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values
        }} />
        <Box m={1}>
         <Grid
            container
            justify="center"
          >
        {editable ?  <Grid
          item
          xs={2}
        >
          <Button
              variant="contained"
              size="small"
              onClick={()=> {
                handleReset();
                setIsEditable(!isEditable)
              }}
            >
              <SvgIcon
                fontSize="small"
                className={classes.actionIcon}
              >
                <EditIcon />
              </SvgIcon>
              { isEditable? "Cancel" : "Edit" }
         </Button>
           </Grid>: null }
        { editable && isEditable ? 
          (<Grid
            item
            xs={2}
            >
              <Button
                color="secondary"
                disabled={isSubmitting}
                size="small"
                type="submit"
                variant="contained"
                startIcon={<SaveIcon />}
              >
                Save
              </Button>
            </Grid>)
            :null }
          </Grid>
          </Box>
      </form>
      )}
    </Formik>
      </Card>
    );
  }

  export default CustomInfoComponent;
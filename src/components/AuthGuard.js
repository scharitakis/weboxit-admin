import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useIdentityContext } from 'react-netlify-identity-widget'
import { useHistory } from 'react-router';

function AuthGuard({ children }) {
  //const account = useSelector((state) => state.account);
  const identity  = useIdentityContext()
  const history = useHistory()

  if (!identity.isLoggedIn) {
    return <Redirect to="/login" />;
  }

  const logmeout = async ()=>{
    try{
      //console.log(identity)
      await identity.logoutUser()
      history.push('/login');
    }catch(e){
     // console.log(e)
      history.push('/login');
    }
  }

  if(identity.user && !identity.user.app_metadata.roles){
    logmeout()
    return null;
    //return <Redirect to="/login" />
  }

  return children;
}

AuthGuard.propTypes = {
  children: PropTypes.any
};

export default AuthGuard;

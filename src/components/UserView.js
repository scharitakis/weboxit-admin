import React from 'react';
import PropTypes from 'prop-types';
import useUserAs from 'src/hooks/useUserAs'

function UserView({ children }) {
  const {userAs} = useUserAs()
  
  if(userAs.userAs){
    return children;
  }

  return null;
}

UserView.propTypes = {
  children: PropTypes.any
};

export default UserView;

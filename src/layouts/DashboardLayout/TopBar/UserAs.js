import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  CircularProgress,
  Drawer,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  TextField,
  Tooltip,
  Typography,
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import {
  Search as SearchIcon,
  XCircle as XIcon
} from 'react-feather';
import axios from 'src/utils/axios';
import useUserAs from 'src/hooks/useUserAs'
import { useLazyQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const useStyles = makeStyles(() => ({
  drawer: {
    width: 500,
    maxWidth: '100%'
  }
}));

const GET_ACCOUNTS_BY_USERNAME = gql`    
  query FindAccountsByUserName($username:String!) {
    findAccountByUsername(username:$username){
  	data{ 
      username
      _id
      _ts
      contact{
        fullname
        phone
        email
      }
    }
  }
}`;

const SearchBox = ({handleSearch})=>{
  const classes = useStyles();
  const [value, setValue] = useState('');
  return (
    <>
         <Box mt={2}>
              <TextField
                className={classes.queryField}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        fontSize="small"
                        color="action"
                      >
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                onChange={(event) => setValue(event.target.value)}
                onKeyPress={(event)=>{if(event.key === "Enter"){
                  handleSearch(value)
                }}}
                placeholder="Search a weboxit user via email"
                value={value}
                variant="outlined"
              />
            </Box>
            <Box
              mt={2}
              display="flex"
              justifyContent="flex-end"
            >
              <Button
                color="secondary"
                variant="contained"
                onClick={()=> handleSearch(value)}
              >
                Search
              </Button>
            </Box>
    </>
  )
}

function Search() {
  const classes = useStyles();
  const { userAs, setUserAs } = useUserAs();
 
  const [isOpen, setOpen] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [results, setResults] = useState(null);
  const { enqueueSnackbar } = useSnackbar();

  const [getAccount, { loading, data }] = useLazyQuery(GET_ACCOUNTS_BY_USERNAME,{
    fetchPolicy: 'no-cache',
    onCompleted: data => {
      setLoading(false);
      setResults(data.findAccountByUsername.data);
    },
    onError: (err)=>{
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
      setLoading(false);
    }
  });

  const handleOpen = () => {
    setResults(null)
    setOpen(true);
  };

  const handleClose = () => {
    setResults(null)
    setOpen(false);
  };

  const handleSearch = async (value) => {
    try {
      setResults(null);
      setLoading(true);
      getAccount({ variables: { username: value}})
    } catch (error) {
      enqueueSnackbar('Something went wrong', {
        variant: 'error'
      });
      setLoading(false);
    }
  };

  //console.log('results', results)

  return (
    <>
      <Typography
                variant="h6"
                color="white"
      >
        User As: { userAs && userAs.userAs? userAs.userAs.username : '' }
      </Typography>
      <Tooltip title="User as: ">
        <IconButton
          color="inherit"
          onClick={handleOpen}
        >
          <SvgIcon fontSize="small">
            <SearchIcon />
          </SvgIcon>
        </IconButton>
      </Tooltip>
      <Drawer
        anchor="right"
        classes={{ paper: classes.drawer }}
        ModalProps={{ BackdropProps: { invisible: true } }}
        onClose={handleClose}
        open={isOpen}
        variant="temporary"
      >
        <PerfectScrollbar options={{ suppressScrollX: true }}>
          <Box p={3}>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography
                variant="h4"
                color="textPrimary"
              >
                Connect as User
              </Typography>
              <IconButton onClick={handleClose}>
                <SvgIcon fontSize="small">
                  <XIcon />
                </SvgIcon>
              </IconButton>
            </Box>
            <SearchBox handleSearch={handleSearch}/>
            <Box mt={4}>
              {isLoading ? (
                <Box
                  display="flex"
                  justifyContent="center"
                >
                  <CircularProgress />
                </Box>
              ) : (
                <>
                  {results && results.length > 0 && results.map((result) => (
                    <Box key={`${result._id}`}>
                       <Grid
                        container
                        direction="row"
                        spacing={1}
                      >
                      <Grid
                        item
                        xs={6}
                      >
                        <Typography
                          variant="body2"
                          color="textPrimary"
                        >
                          {result.username}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                      >
                      <Button
                        color="secondary"
                        variant="contained"
                        onClick={()=>{
                          setUserAs({userAs:result});
                          handleClose()
                        }}
                      >
                        Use User
                      </Button>
                      </Grid>
                      </Grid>
                    </Box>
                  ))}
                </>
              )}
            </Box>
            
          </Box>
        </PerfectScrollbar>
      </Drawer>
    </>
  );
}

export default Search;

const { ApolloServer, ApolloError, gql } = require('apollo-server-lambda');
const { createHttpLink } = require('apollo-link-http');
const fetch = require('node-fetch');
const soapRequest = require('easy-soap-request');
const parser = require('fast-xml-parser');
const bwipjs = require('bwip-js');
const { v4: uuidv4 } = require('uuid');
const dayjs = require('dayjs')
const https = require('https');

// set this inorder to use self-signed certificate on your proxy
https.globalAgent.options.rejectUnauthorized = false;

const {
 // introspectSchema,
  makeExecutableSchema,
  makeRemoteExecutableSchema,
 // addMockFunctionsToSchema,
  mergeSchemas
} = require('graphql-tools');

const { GraphQLClient } = require('graphql-request')

const sgMail = require('@sendgrid/mail');

const { typeDefs } = require('./schema.js');
const faunaEndpoint = 'https://graphql.fauna.com/graphql'

const TWILIO_ACCOUNT_SID = 'AC11165d5d6762586fafb5c4f387b04b02'; 
const TWILIO_AUTH_TOKEN = '9530c7e2338b667d1e726eb3d8ac8424'; 
const TWILIO_VERIFY_SERVICE = 'VA95c086dbe07c122fe74be16155a0e540'

const client = require('twilio')(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

const SENDGRID_API_KEY = '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';


const EMAIL_TEMPLATES = {
  RECEIVER:"d-d646865967114bf9a12b4a3ecb463344",
  SENDER:"d-cdfd140e42a4490381aa8d50a2c7634c",
}

sgMail.setApiKey(SENDGRID_API_KEY);



//SOAP CONFIGURATION 
//const BankSoapUrl = 'https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx';

const BankSoapUrl = 'https://64.225.93.216:8889/services/tickets/issuer.asmx';

const BankSoapHeaders = {
  'user-agent': 'weboxit-soap-request',
  'Content-Type': 'text/xml;charset=UTF-8',
  SOAPAction: 'http://piraeusbank.gr/paycenter/redirection/IssueNewTicket',
};

//const BANK_PASSWORD_HASH = crypto.createHash('md5').update("LU586529").digest("hex");

const BANK_PROVIDED_PARAMETERS = {
  AcquirerId: 14,
  MerchantId: 2141308013,
  PosId: 2130861870,
  Username:"SE266119",
  Password:"32472d36d2fd4fe7a24b811a04458a4c"
}

const createSoapInvitationRequest = (data)=>{
		
	const xml = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <IssueNewTicket xmlns="http://piraeusbank.gr/paycenter/redirection">
      <Request>
        <Username>${data.Username}</Username>
        <Password>${data.Password}</Password>
        <MerchantId>${data.MerchantId}</MerchantId>
        <PosId>${data.PosId}</PosId>
        <AcquirerId>${data.AcquirerId}</AcquirerId>
        <MerchantReference>${data.MerchantReference}</MerchantReference>
        <RequestType>02</RequestType>
        <ExpirePreauth>0</ExpirePreauth>
        <Amount>${data.Amount}</Amount>
        <CurrencyCode>978</CurrencyCode>
        <Installments>0</Installments>
        <Bnpl>0</Bnpl>
        <Parameters>${data.Parameters}</Parameters>
      </Request>
    </IssueNewTicket>
  </soap:Body>
</soap:Envelope>`

	return xml;
	
}

const soapResolver = (result) => {
	let error = null;
	if(result.ResultCode !== 0){
		error = result.ResultDescription
  }
  const res = {
    ResultCode: result.ResultCode,
    ResultDescription: result.ResultDescription,
    TranTicket: result.TranTicket,
    Timestamp: result.Timestamp, //TODO: proper timestamp
    MinutesToExpiration: result.MinutesToExpiration
  }
	
	return [res, error]
}


const sendSOAP = (data) => {
  return new Promise(async (resolve, reject) => {

    console.log('starting Soap')
    const params = {
      AcquirerId: BANK_PROVIDED_PARAMETERS.AcquirerId,
      MerchantId: BANK_PROVIDED_PARAMETERS.MerchantId,
      PosId: BANK_PROVIDED_PARAMETERS.PosId,
      Username: BANK_PROVIDED_PARAMETERS.Username,
      Password: BANK_PROVIDED_PARAMETERS.Password,
      MerchantReference: uuidv4(),
      Amount: data.totalCost, // 10.12
      Parameters: data.parameters || ''// "p1=p2"
    }
    
    
    try{
      const xml = createSoapInvitationRequest(params);

      console.log('xml', xml);

      const { response } = await soapRequest({ url: BankSoapUrl, headers: BankSoapHeaders, xml});
      const { body, statusCode } = response;
      if(statusCode === 200){
        const parsedData = parser.parse(body)
        const result = parsedData['soap:Envelope']['soap:Body']['IssueNewTicketResponse']['IssueNewTicketResult'];
        const [data,error] = soapResolver(result);
        console.log('data',data)
        resolve([{...params, ...data}, error]);
      }
    }catch(e){
      console.log('sendSOAP ',e);
      reject([null, e]);
    }
  })
  }

// const createTemplate = (boxes) => {
//   const html = `<html>
//   <head>
//   </head>
//   <body>
//   ${boxes.map(b=> `<div>${b.name}</div>`)}
//   </body>
//   </html>
//   `
//   return html;
// }

// const generateBarcodes = value =>  bwipjs.toBuffer({
//       bcid: 'code128', // Barcode type
//       text: value, // Text to encode
//       scale: 2, // 3x scaling factor
//       height: 10, // Bar height, in millimeters
//       includetext: false, // Show human-readable text
//       textxalign: 'center', // Always good to set this      // Always good to set this
//     }); 

const PAYMENT_STATUS = {
  completed:'completed',
  pending:'pending',
  rejected:'rejected'
}

exports.handler = async function(event, context) {
  const {user} = context.clientContext;
  //console.log('user',user)

  // uncomment the below to restrict access and allow only to admin
  // if(!user || !user.app_metadata.roles ){
  //   return {
  //     statusCode: 401,
  //     body: "Need to login",
  //   };
  // }

  /** required for Fauna GraphQL auth */
  if (!process.env.FAUNADB_SERVER_SECRET) {
    const msg = `
    FAUNADB_SERVER_SECRET missing. 
    Did you forget to install the fauna addon or forgot to run inside Netlify Dev?
    `;
    console.error(msg);
    return {
      statusCode: 500,
      body: JSON.stringify({ msg }),
    };
  }

  const b64encodedSecret = Buffer.from(
    process.env.FAUNADB_SERVER_SECRET + ':' // weird but they
  ).toString('base64');
  const headers = { Authorization: `Basic ${b64encodedSecret}` };

  /** standard creation of apollo-server executable schema */
  const link = createHttpLink({
    uri: faunaEndpoint, // modify as you see fit
    fetch,
    headers,
  });

 
  const graphQLClient = new GraphQLClient(faunaEndpoint, {
    headers
  })

  const CREATE_PURCHASE_MUTATION = /* GraphQL */ `
    mutation CreatePurchase(
      $accountId: ID!, 
      $boxes: [ID!],
      $merchantRef: String!,
      $status: PaymentStatus!,
      $totalCost: Float!,
      $ResultCode: Int,
      $ResultDescription: String,     
      $MinutesToExpiration: Int,
      $MerchantReference: String,
      $Parameters: String,
      $Amount: Float,
      $TranTicket: String,
      $Timestamp: Long
      ) {
      createPurchase(
        data:{
            totalCost: $totalCost
            merchantRef: $merchantRef
            boxes: $boxes
            account: { connect: $accountId }
            status: $status
            transTicket:{          
              ResultCode: $ResultCode 
              ResultDescription: $ResultDescription    
              MinutesToExpiration: $MinutesToExpiration
              MerchantReference: $MerchantReference
              Parameters: $Parameters 
              Amount: $Amount
              TranTicket: $TranTicket
              Timestamp: $Timestamp
            }
          }
      )
      {
        _id
        account{
          _id
        },
        merchantRef
      }
    }
  `


  const GqlClientSchemaResolver = {
    Query:{
      validatePin: async (obj, args, context, info)=>{
        try{ 
          const verifyPin_sms = await client.verify.services(TWILIO_VERIFY_SERVICE)
          .verificationChecks
          .create({to: `+30${args.phone}`, code: args.pin})

          if(verifyPin_sms.status == 'approved'){
            return true
          }
          return false
        }catch(e){
          console.log(e);
          return false
        }
      }
    },
    Mutation: {
      requestPin: async (obj, args, context, info)=>{
        try{
          const sendPin_sms = await client.verify.services(TWILIO_VERIFY_SERVICE)
             .verifications 
             .create({to: `+30${args.phone}`, channel: 'sms'})
          return true
        }catch(e){
          console.log(e);
          return false
        }
      },
      sendEmail: async (obj, args, context, info)=>{
        const msg = {
          to: args.email,
          from: 'info@weboxit.com',
          subject: 'Weboxit Send Email',
          html: args.html,
        };
        try {
          await sgMail.send(msg);
          return true;
        } catch (err) {
          console.error(err.toString());
          return false
        }
      },
      sendEmailTemplate: async (obj, args, context, info)=>{
        const {data:{email, emailTemplate, templateData}} = args;
        // const boxData = await Promise.all(boxes.map(async (b)=>{
        //   const brcode = await generateBarcodes(b.id);
        //   b.url = `data:image/png;base64,${brcode.toString('base64')}`;
        //   return b;
        // })
        // )

        
        const msg = {
          to: email,
          from: 'info@weboxit.com',
          templateId: EMAIL_TEMPLATES[emailTemplate],
          dynamic_template_data: templateData,
        };
        try {
          await sgMail.send(msg);
          return true;
        } catch (err) {
          console.error(err.toString());
          return false
        }
      },
      createPurchaseTicket: async (obj, args, context, info)=>{
        try{
          //console.log('args', args);

           //TODO; implement the SOAP call + the save the PurcaseTicket if successsfull

          const [res, error] = await sendSOAP(args.data)
          //console.log('soap res', res)
          //console.log('soap error', error)
       
          //Create a purchase Here 
          const data = await graphQLClient.request(CREATE_PURCHASE_MUTATION, {
            accountId: args.data.accountId, 
            boxes: args.data.boxIds,
            merchantRef: res.MerchantReference,
            status: res.ResultCode !== 0 ? PAYMENT_STATUS.rejected : PAYMENT_STATUS.pending,
            totalCost: res.Amount,
            ResultCode: res.ResultCode,
            ResultDescription: res.ResultDescription,     
            MinutesToExpiration: res.MinutesToExpiration,
            MerchantReference: res.MerchantReference,
            Parameters: res.Parameters,   
            Amount: res.Amount,
            TranTicket: res.TranTicket,
            Timestamp: dayjs(res.Timestamp).valueOf()
          })

          console.log('fauna data', data);

          if(error){
            //console.log(error)
            throw new ApolloError(error,res.ResultCode);
          } 

          // If no error create a PurcaseTicket with the all the results
          //need to return the below values to the UI inorder to make a post Request
          return  {
            AcquirerId: BANK_PROVIDED_PARAMETERS.AcquirerId,
            MerchantId: BANK_PROVIDED_PARAMETERS.MerchantId ,
            PosId: BANK_PROVIDED_PARAMETERS.PosId,
            User: BANK_PROVIDED_PARAMETERS.Username,
            LanguageCode: 'el-GR',
            MerchantReference: data.createPurchase.merchantRef,
            ParamBackLink: ''
          }
          
        }catch(e){
          console.log('createPurchaseTicket error', e)
          return e
        }
      }
    } 
  }

  const GqlClientSchema = makeExecutableSchema({
    typeDefs: `
      type Query{
        nothing: Boolean
        validatePin(phone: String!, email: String!, pin: String!): Boolean
      }

      input EmailBoxesInput{
        name: String
        size: String
        address: String
        id: String
      }

      input EmailTemplateData{
        name: String
        boxes: [EmailBoxesInput]
      }

      input EmailTemplateInput{
        email: String
        emailTemplate: String
        templateData: EmailTemplateData
      }

      input TicketInput{
        totalCost: Float
        accountId: ID
        boxIds:[ID!]
      }

      type PurchaseTicket{
        AcquirerId: Int
        MerchantId: Int 
        PosId: Int
        User: String,
        LanguageCode: String
        MerchantReference: String
        ParamBackLink: String
      }

      type Mutation {
        requestPin(phone: String!, email: String!): Boolean
        sendEmail(email: String!, html: String!): Boolean
        sendEmailTemplate(data: EmailTemplateInput): Boolean
        createPurchaseTicket(data:TicketInput): PurchaseTicket
      }
    `,
    resolvers:GqlClientSchemaResolver
  });


  const faunaSchema = makeExecutableSchema({ typeDefs: typeDefs });
  const executableSchema = makeRemoteExecutableSchema({
    schema:faunaSchema,
    link,
  });

  const server = new ApolloServer({
    schema: mergeSchemas({
      schemas: [executableSchema, GqlClientSchema],
    }),
    playground: process.env.NETLIFY_DEV? Boolean(process.env.NETLIFY_DEV): false,
    introspection: true,
  });
  return new Promise((yay, nay) => {
    const cb = (err, args) => (err ? nay(err) : yay(args));
    server.createHandler()(event, context, cb);
  });
};

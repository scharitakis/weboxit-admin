module.exports.typeDefs = `directive @embedded on OBJECT
directive @collection(name: String!) on OBJECT
directive @index(name: String!) on FIELD_DEFINITION
directive @resolver(
  name: String
  paginated: Boolean! = false
) on FIELD_DEFINITION
directive @relation(name: String) on FIELD_DEFINITION
directive @unique(index: String) on FIELD_DEFINITION
type Account {
  companyInfo: CompanyInfo
  username: String!
  # The document's ID.
  _id: ID!
  purchases: [Purchase]
  contact: Contact!
  boxes(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
  ): BoxPage!
  isEnabled: Boolean
  type: String
  termsAccepted: Boolean!
  password: String!
  # The document's timestamp.
  _ts: Long!
}

# Allow manipulating the relationship between the types 'Account' and 'Box'.
input AccountBoxesRelation {
  # Create one or more documents of type 'Box' and associate them with the current document.
  create: [BoxInput]
  # Connect one or more documents of type 'Box' with the current document using their IDs.
  connect: [ID]
  # Disconnect the given documents of type 'Box' from the current document using their IDs.
  disconnect: [ID]
}

# 'Account' input values
input AccountInput {
  username: String!
  password: String!
  contact: ContactInput!
  termsAccepted: Boolean!
  type: String
  companyInfo: CompanyInfoInput
  isEnabled: Boolean
  purchases: [ID]
  boxes: AccountBoxesRelation
}

# The pagination object for elements of type 'Account'.
type AccountPage {
  # The elements of type 'Account' in this page.
  data: [Account]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

type Address {
  street: String!
  zipCode: String!
  city: String!
}

# 'Address' input values
input AddressInput {
  street: String!
  zipCode: String!
  city: String!
}

type AdminAccount {
  role: String
  username: String!
  # The document's ID.
  _id: ID!
  contact: Contact
  isEnabled: Boolean
  created: Long
  password: String!
  # The document's timestamp.
  _ts: Long!
}

# 'AdminAccount' input values
input AdminAccountInput {
  username: String!
  password: String!
  role: String
  contact: ContactInput
  isEnabled: Boolean
  created: Long
}

type Box {
  recipient: Contact!
  pickupPoint: Station
  deletedAt: Long
  deliveryPoint: Station
  size: BoxSize!
  history: [Tracking]
  # The document's ID.
  _id: ID!
  shippingCost: Float
  isDeleted: Boolean
  status: BoxStatus!
  comment: String
  createdAt: Long
  account: Account
  purchase: Purchase
  delivery: Delivery
  deliveryAtHome: Boolean
  # The document's timestamp.
  _ts: Long!
}

# Allow manipulating the relationship between the types 'Box' and 'Account' using the field 'Box.account'.
input BoxAccountRelation {
  # Create a document of type 'Account' and associate it with the current document.
  create: AccountInput
  # Connect a document of type 'Account' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Account'
  disconnect: Boolean
}

# Allow manipulating the relationship between the types 'Box' and 'Station' using the field 'Box.deliveryPoint'.
input BoxDeliveryPointRelation {
  # Create a document of type 'Station' and associate it with the current document.
  create: StationInput
  # Connect a document of type 'Station' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Station'
  disconnect: Boolean
}

# Allow manipulating the relationship between the types 'Box' and 'Delivery' using the field 'Box.delivery'.
input BoxDeliveryRelation {
  # Create a document of type 'Delivery' and associate it with the current document.
  create: DeliveryInput
  # Connect a document of type 'Delivery' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Delivery'
  disconnect: Boolean
}

# 'Box' input values
input BoxInput {
  account: BoxAccountRelation
  size: BoxSize!
  recipient: ContactInput!
  deliveryPoint: BoxDeliveryPointRelation
  pickupPoint: BoxPickupPointRelation
  deliveryAtHome: Boolean
  shippingCost: Float
  purchase: BoxPurchaseRelation
  delivery: BoxDeliveryRelation
  status: BoxStatus!
  comment: String
  history: [ID]
  createdAt: Long
  isDeleted: Boolean
  deletedAt: Long
}

# The pagination object for elements of type 'Box'.
type BoxPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# Allow manipulating the relationship between the types 'Box' and 'Station' using the field 'Box.pickupPoint'.
input BoxPickupPointRelation {
  # Create a document of type 'Station' and associate it with the current document.
  create: StationInput
  # Connect a document of type 'Station' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Station'
  disconnect: Boolean
}

# Allow manipulating the relationship between the types 'Box' and 'Purchase' using the field 'Box.purchase'.
input BoxPurchaseRelation {
  # Create a document of type 'Purchase' and associate it with the current document.
  create: PurchaseInput
  # Connect a document of type 'Purchase' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Purchase'
  disconnect: Boolean
}

enum BoxSize {
  no_2
  no_5
  no_10
  no_15
}

enum BoxStatus {
  pending
  paid
  scanned
  shipped
  inTransit
  dropped
  delivered
  not_delivered
  refunded
}

type CompanyInfo {
  companyName: String
  vat: String
  taxOffice: String
  POBox: String
}

# 'CompanyInfo' input values
input CompanyInfoInput {
  companyName: String
  vat: String
  taxOffice: String
  POBox: String
}

type Contact {
  fullname: String!
  address: Address!
  phone: String!
  email: String
}

# 'Contact' input values
input ContactInput {
  fullname: String!
  address: AddressInput!
  phone: String!
  email: String
}

type Courier {
  name: String!
  # The document's ID.
  _id: ID!
  contact: Contact!
  isEnabled: Boolean
  pin: String
  # The document's timestamp.
  _ts: Long!
}

# 'Courier' input values
input CourierInput {
  name: String!
  contact: ContactInput!
  pin: String
  isEnabled: Boolean
}

# The pagination object for elements of type 'Courier'.
type CourierPage {
  # The elements of type 'Courier' in this page.
  data: [Courier]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

scalar Date

type Delivery {
  # The document's ID.
  _id: ID!
  # The document's timestamp.
  _ts: Long!
  boxes: [Box]
}

# 'Delivery' input values
input DeliveryInput {
  boxes: [ID]
}

# The 'Long' scalar type represents non-fractional signed whole numeric values.
# Long can represent values between -(2^63) and 2^63 - 1.
scalar Long

type Mutation {
  # Delete an existing document in the collection of 'Delivery'
  deleteDelivery(
    # The 'Delivery' document's ID
    id: ID!
  ): Delivery
  # Create a new document in the collection of 'Tracking'
  createTracking(
    # 'Tracking' input values
    data: TrackingInput!
  ): Tracking!
  # Delete an existing document in the collection of 'Purchase'
  deletePurchase(
    # The 'Purchase' document's ID
    id: ID!
  ): Purchase
  # Create a new document in the collection of 'Courier'
  createCourier(
    # 'Courier' input values
    data: CourierInput!
  ): Courier!
  # Update an existing document in the collection of 'AdminAccount'
  updateAdminAccount(
    # The 'AdminAccount' document's ID
    id: ID!
    # 'AdminAccount' input values
    data: AdminAccountInput!
  ): AdminAccount
  # Delete an existing document in the collection of 'Box'
  deleteBox(
    # The 'Box' document's ID
    id: ID!
  ): Box
  # Update an existing document in the collection of 'SFAuthAccountPin'
  updateSFAuthAccountPin(
    # The 'SFAuthAccountPin' document's ID
    id: ID!
    # 'SFAuthAccountPin' input values
    data: SFAuthAccountPinInput!
  ): SFAuthAccountPin
  # Delete an existing document in the collection of 'SFAuthAccountPin'
  deleteSFAuthAccountPin(
    # The 'SFAuthAccountPin' document's ID
    id: ID!
  ): SFAuthAccountPin
  # Update an existing document in the collection of 'Box'
  updateBox(
    # The 'Box' document's ID
    id: ID!
    # 'Box' input values
    data: BoxInput!
  ): Box
  # Update an existing document in the collection of 'Delivery'
  updateDelivery(
    # The 'Delivery' document's ID
    id: ID!
    # 'Delivery' input values
    data: DeliveryInput!
  ): Delivery
  # Update an existing document in the collection of 'Station'
  updateStation(
    # The 'Station' document's ID
    id: ID!
    # 'Station' input values
    data: StationInput!
  ): Station
  # Create a new document in the collection of 'AdminAccount'
  createAdminAccount(
    # 'AdminAccount' input values
    data: AdminAccountInput!
  ): AdminAccount!
  # Delete an existing document in the collection of 'Account'
  deleteAccount(
    # The 'Account' document's ID
    id: ID!
  ): Account
  # Delete an existing document in the collection of 'AdminAccount'
  deleteAdminAccount(
    # The 'AdminAccount' document's ID
    id: ID!
  ): AdminAccount
  # Update an existing document in the collection of 'Account'
  updateAccount(
    # The 'Account' document's ID
    id: ID!
    # 'Account' input values
    data: AccountInput!
  ): Account
  # Create a new document in the collection of 'Box'
  createBox(
    # 'Box' input values
    data: BoxInput!
  ): Box!
  # Update an existing document in the collection of 'Courier'
  updateCourier(
    # The 'Courier' document's ID
    id: ID!
    # 'Courier' input values
    data: CourierInput!
  ): Courier
  updatePurchaseBoxesStatus(purchaseId: ID!, boxStatus: String!): [Box!]
  # Delete an existing document in the collection of 'Courier'
  deleteCourier(
    # The 'Courier' document's ID
    id: ID!
  ): Courier
  payBoxes(boxIds: [ID!]): [Box!]
  # Create a new document in the collection of 'Purchase'
  createPurchase(
    # 'Purchase' input values
    data: PurchaseInput!
  ): Purchase!
  # Update an existing document in the collection of 'Purchase'
  updatePurchase(
    # The 'Purchase' document's ID
    id: ID!
    # 'Purchase' input values
    data: PurchaseInput!
  ): Purchase
  # Create a new document in the collection of 'Account'
  createAccount(
    # 'Account' input values
    data: AccountInput!
  ): Account!
  # Delete an existing document in the collection of 'Station'
  deleteStation(
    # The 'Station' document's ID
    id: ID!
  ): Station
  # Update an existing document in the collection of 'Tracking'
  updateTracking(
    # The 'Tracking' document's ID
    id: ID!
    # 'Tracking' input values
    data: TrackingInput!
  ): Tracking
  # Delete an existing document in the collection of 'Tracking'
  deleteTracking(
    # The 'Tracking' document's ID
    id: ID!
  ): Tracking
  # Create a new document in the collection of 'Delivery'
  createDelivery(
    # 'Delivery' input values
    data: DeliveryInput!
  ): Delivery!
  updateAccountPassword(accountId: ID!, password: String!): Account
  # Create a new document in the collection of 'SFAuthAccountPin'
  createSFAuthAccountPin(
    # 'SFAuthAccountPin' input values
    data: SFAuthAccountPinInput!
  ): SFAuthAccountPin!
  updateBoxTrackingInfo(
    stationId: String!
    boxIds: [ID!]
    status: String!
    validation: String!
  ): [Box!]
  # Create a new document in the collection of 'Station'
  createStation(
    # 'Station' input values
    data: StationInput!
  ): Station!
  purchaseBoxes(accountId: ID!, totalCost: Float!, boxIds: [ID!]): [Box!]
}

enum PaymentStatus {
  completed
  pending
  rejected
}

type Purchase {
  totalCost: Float!
  transResponse: TransPostResponse
  # The document's ID.
  _id: ID!
  transTicket: TransTicket
  boxes: [Box!]
  status: PaymentStatus!
  merchantRef: String!
  account: Account
  # The document's timestamp.
  _ts: Long!
}

# Allow manipulating the relationship between the types 'Purchase' and 'Account' using the field 'Purchase.account'.
input PurchaseAccountRelation {
  # Create a document of type 'Account' and associate it with the current document.
  create: AccountInput
  # Connect a document of type 'Account' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Account'
  disconnect: Boolean
}

# 'Purchase' input values
input PurchaseInput {
  account: PurchaseAccountRelation
  boxes: [ID!]
  totalCost: Float!
  merchantRef: String!
  status: PaymentStatus!
  transTicket: TransTicketInput
  transResponse: TransPostResponseInput
}

# The pagination object for elements of type 'Purchase'.
type PurchasePage {
  # The elements of type 'Purchase' in this page.
  data: [Purchase]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

type Query {
  allBoxes(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
  ): BoxPage!
  # Find a document from the collection of 'Box' by its id.
  findBoxByID(
    # The 'Box' document's ID
    id: ID!
  ): Box
  getAccountsByType(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    type: String!
  ): QueryGetAccountsByTypePage!
  totalStats: TotalStatsObj!
  findPendingBoxesByAccountIdAsc(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
  ): QueryFindPendingBoxesByAccountIdAscPage!
  findBoxByAccountNStatus(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
    status: BoxStatus!
  ): QueryFindBoxByAccountNStatusPage!
  findBoxByAccountNStatusRev(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
    status: BoxStatus!
  ): QueryFindBoxByAccountNStatusRevPage!
  # Find a document from the collection of 'Purchase' by its id.
  findPurchaseByID(
    # The 'Purchase' document's ID
    id: ID!
  ): Purchase
  validateRemoveSFAuthPin(phone: String!, pin: String!): Boolean
  boxesByAccountIdNotDeletedAsc(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
  ): QueryBoxesByAccountIdNotDeletedAscPage!
  # Find a document from the collection of 'AdminAccount' by its id.
  findAdminAccountByID(
    # The 'AdminAccount' document's ID
    id: ID!
  ): AdminAccount
  findBoxesByStatusAndPickupPoint(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    status: BoxStatus!
    pickupPoint: ID!
  ): QueryFindBoxesByStatusAndPickupPointPage!
  findBoxesByStatusAndDeliveryPoint(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    status: BoxStatus!
    deliveryPoint: ID!
  ): QueryFindBoxesByStatusAndDeliveryPointPage!
  boxesByAccountIdAsc(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
  ): QueryBoxesByAccountIdAscPage!
  # Find a document from the collection of 'Courier' by its id.
  findCourierByID(
    # The 'Courier' document's ID
    id: ID!
  ): Courier
  getCourierByPin(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    pin: String!
  ): QueryGetCourierByPinPage!
  # Find a document from the collection of 'SFAuthAccountPin' by its id.
  findSFAuthAccountPinByID(
    # The 'SFAuthAccountPin' document's ID
    id: ID!
  ): SFAuthAccountPin
  allStations(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
  ): StationPage!
  # Find a document from the collection of 'Station' by its id.
  findStationByID(
    # The 'Station' document's ID
    id: ID!
  ): Station
  findAccountByUsername(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    username: String!
  ): QueryFindAccountByUsernamePage!
  # Find a document from the collection of 'Tracking' by its id.
  findTrackingByID(
    # The 'Tracking' document's ID
    id: ID!
  ): Tracking
  findPurchaseByAccountNStatus(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
    status: PaymentStatus!
  ): QueryFindPurchaseByAccountNStatusPage!
  # Find a document from the collection of 'Delivery' by its id.
  findDeliveryByID(
    # The 'Delivery' document's ID
    id: ID!
  ): Delivery
  allPurchases(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
  ): PurchasePage!
  findBoxByAccountNStatusNotDeletedRev(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    accountId: ID!
    status: BoxStatus!
  ): QueryFindBoxByAccountNStatusNotDeletedRevPage!
  allAccounts(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
  ): AccountPage!
  # Find a document from the collection of 'Account' by its id.
  findAccountByID(
    # The 'Account' document's ID
    id: ID!
  ): Account
  allCouriers(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
  ): CourierPage!
  findPurchaseByMerchantRef(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    merchantRef: String!
  ): QueryFindPurchaseByMerchantRefPage!
  validateSFAuthPin(
    # The number of items to return per page.
    _size: Int
    # The pagination cursor.
    _cursor: String
    phone: String!
    pin: String!
  ): QueryValidateSFAuthPinPage!
}

# The pagination object for elements of type 'Box'.
type QueryBoxesByAccountIdAscPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryBoxesByAccountIdNotDeletedAscPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Account'.
type QueryFindAccountByUsernamePage {
  # The elements of type 'Account' in this page.
  data: [Account]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryFindBoxByAccountNStatusNotDeletedRevPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryFindBoxByAccountNStatusPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryFindBoxByAccountNStatusRevPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryFindBoxesByStatusAndDeliveryPointPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryFindBoxesByStatusAndPickupPointPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Box'.
type QueryFindPendingBoxesByAccountIdAscPage {
  # The elements of type 'Box' in this page.
  data: [Box]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Purchase'.
type QueryFindPurchaseByAccountNStatusPage {
  # The elements of type 'Purchase' in this page.
  data: [Purchase]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Purchase'.
type QueryFindPurchaseByMerchantRefPage {
  # The elements of type 'Purchase' in this page.
  data: [Purchase]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Account'.
type QueryGetAccountsByTypePage {
  # The elements of type 'Account' in this page.
  data: [Account]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'Courier'.
type QueryGetCourierByPinPage {
  # The elements of type 'Courier' in this page.
  data: [Courier]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

# The pagination object for elements of type 'SFAuthAccountPin'.
type QueryValidateSFAuthPinPage {
  # The elements of type 'SFAuthAccountPin' in this page.
  data: [SFAuthAccountPin]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

type SFAuthAccountPin {
  email: String
  # The document's ID.
  _id: ID!
  pin: String!
  phone: String!
  # The document's timestamp.
  _ts: Long!
}

# 'SFAuthAccountPin' input values
input SFAuthAccountPinInput {
  phone: String!
  email: String
  pin: String!
}

type Station {
  name: String!
  lng: String!
  # The document's ID.
  _id: ID!
  contact: Contact!
  isEnabled: Boolean
  pin: String
  lat: String!
  # The document's timestamp.
  _ts: Long!
}

# 'Station' input values
input StationInput {
  name: String!
  contact: ContactInput!
  lat: String!
  lng: String!
  pin: String
  isEnabled: Boolean
}

# The pagination object for elements of type 'Station'.
type StationPage {
  # The elements of type 'Station' in this page.
  data: [Station]!
  # A cursor for elements coming after the current page.
  after: String
  # A cursor for elements coming before the current page.
  before: String
}

scalar Time

type TotalStatsObj {
  totalBoxes: Int
  totalPurchases: Int
  totalAccounts: Int
}

# 'TotalStatsObj' input values
input TotalStatsObjInput {
  totalBoxes: Int
  totalPurchases: Int
  totalAccounts: Int
}

type Tracking {
  description: String
  # The document's ID.
  _id: ID!
  status: BoxStatus!
  createdAt: Long
  box: Box
  # The document's timestamp.
  _ts: Long!
}

# Allow manipulating the relationship between the types 'Tracking' and 'Box' using the field 'Tracking.box'.
input TrackingBoxRelation {
  # Create a document of type 'Box' and associate it with the current document.
  create: BoxInput
  # Connect a document of type 'Box' with the current document using its ID.
  connect: ID
  # If true, disconnects this document from 'Box'
  disconnect: Boolean
}

# 'Tracking' input values
input TrackingInput {
  box: TrackingBoxRelation
  status: BoxStatus!
  createdAt: Long
  description: String
}

type TransPostResponse {
  TraceID: String
  MerchantReference: String
  HashKey: String
  PackageNo: String
  Parameters: String
  CardType: String
  SupportReferenceID: String
  ResponseCode: String
  RetrievalRef: String
  ResultDescription: String
  ApprovalCode: String
  TransactionId: String
  ResultCode: String
  ResponseDescription: String
  TransactionDateTime: String
  StatusFlag: String
  LanguageCode: String
  AuthStatus: String
  PaymentMethod: String
}

# 'TransPostResponse' input values
input TransPostResponseInput {
  SupportReferenceID: String
  ResultCode: String
  ResultDescription: String
  StatusFlag: String
  ResponseCode: String
  ResponseDescription: String
  LanguageCode: String
  MerchantReference: String
  TransactionDateTime: String
  TransactionId: String
  CardType: String
  PackageNo: String
  ApprovalCode: String
  RetrievalRef: String
  AuthStatus: String
  Parameters: String
  HashKey: String
  PaymentMethod: String
  TraceID: String
}

type TransTicket {
  MinutesToExpiration: Int
  MerchantReference: String
  Parameters: String
  Amount: Float
  TranTicket: String
  ResultDescription: String
  Timestamp: Long
  ResultCode: Int
}

# 'TransTicket' input values
input TransTicketInput {
  MerchantReference: String
  Amount: Float
  Parameters: String
  TranTicket: String
  Timestamp: Long
  ResultCode: Int
  ResultDescription: String
  MinutesToExpiration: Int
}`;
